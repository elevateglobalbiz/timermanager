# TimerManager

The TimerManager offers an abstraction over the timers management and a complete freedom for registering multiple timers at the same timestamp, 
defining different behaviour per timer and deleting any registered timer at any moment.

There are two different implementation with same functionality for the TimerManager, FullTimerManager and TimerManager. In the FullTimerManager 
there is only one registered timer at the Flink's timer service and every other registered timer is handled manually. In comparison the TimerManager registers
more timers in Flink's timer service (those that are at different triggering timestamps).

Three different classes are implemented for each of the two types of TimerManagers that implement KeyedProcess, KeyedBroadcastProcess and KeyedCoProcess functions (FullTimerManagerKeyedProcess,
FullTimerManagerKeyedBroadcastProcess, FullTimerManagerKeyedCoProcess for FullTimerManager and  TimerManagerKeyedProcess,
TimerManagerKeyedBroadcastProcess, TimerManagerKeyedCoProcess for TimerManager respectively).

There are provided examples for both versions of TimerManager in which is shown how to use and utilize the TimerManager(FullTimerManager or TimerManager) in KeyedProcessFunction, KeyedCoProcessFunction 
and KeyedBroadcastProcessFunction.

In the examples the time characteristics are specified to EventTime.
In case of running the jar file in Flink cluster the current specified main class is ExampleFullTimerManagerInKeyedProcess

