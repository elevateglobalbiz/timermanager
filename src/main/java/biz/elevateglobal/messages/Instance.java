package biz.elevateglobal.messages;


import java.io.Serializable;

public interface Instance extends Serializable,Comparable<Instance>{

    public long getTimestamp();

    public String getField(String field);

    public String getKey();

    public int compareTo(Instance i);

}
