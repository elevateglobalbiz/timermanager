package biz.elevateglobal.messages;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Sample implements Instance, Serializable {

    private static long timestamp=0;

    @SerializedName("key")
    private String key;

    @SerializedName("timestamp")
    private long t;

    public Sample()
    {

    }

    public void setKey(String key) {
        this.key = key;
    }

    public Sample(long i)
    {
        t=i;
    }
    public long getTimestamp() {
       return t;
    }

    @Override
    public String getField(String field) {
        return null;
    }

    @Override
    public String getKey() {
        return key;
    }


    @Override
    public int compareTo(Instance o) {
        return Long.compare(this.getTimestamp(),o.getTimestamp());
    }


    @Override
    public String toString() {
        return "message with timestamp = "+getTimestamp()+"; key = "+getKey();
    }
}
