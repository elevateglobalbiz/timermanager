package biz.elevateglobal.source;


import biz.elevateglobal.source.Data.InputData;
import com.google.gson.Gson;
import biz.elevateglobal.messages.Instance;
import biz.elevateglobal.messages.Sample;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DataSource implements SourceFunction<Instance> {

    @Override
    public void run(SourceContext<Instance> ctx) throws Exception {

        Gson gson = new Gson();

        List<Instance> arrayList = Arrays.asList(InputData.inputData).stream()
                .map(line -> gson.fromJson(line, Sample.class)).collect(Collectors.toList());


        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        arrayList.stream()
                .forEach(protocolMessage ->
                {

                    //++count;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    ctx.collect(protocolMessage);
                });

        try {
            //System.out.println(count);
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void cancel() {

    }
}