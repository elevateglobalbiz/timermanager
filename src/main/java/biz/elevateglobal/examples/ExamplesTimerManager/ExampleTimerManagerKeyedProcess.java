package biz.elevateglobal.examples.ExamplesTimerManager;

import biz.elevateglobal.timerManagerPackage.TimerManager.KeyedProcessTimerManager.TestTimerManagerKeyedProcessFunction;
import biz.elevateglobal.messages.Instance;
import biz.elevateglobal.source.DataSource;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;

import javax.annotation.Nullable;


public class ExampleTimerManagerKeyedProcess {

    final static boolean EVENT_TIME= true;

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(4);

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        DataStream<Instance> dataSource = env.addSource(new DataSource())
                .assignTimestampsAndWatermarks(new AssignerWithPunctuatedWatermarks<Instance>() {
                    @Nullable
                    @Override
                    public Watermark checkAndGetNextWatermark(Instance lastElement, long extractedTimestamp) {
                        return new Watermark(extractedTimestamp);
                    }

                    @Override
                    public long extractTimestamp(Instance element, long previousElementTimestamp) {
                        return element.getTimestamp();
                    }
                });

        dataSource
                .keyBy(Instance::getKey)
                .process(new TestTimerManagerKeyedProcessFunction(EVENT_TIME))
                .print();

        env.execute("timer-manager");


    }
}
