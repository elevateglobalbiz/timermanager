package biz.elevateglobal.examples.ExamplesTimerManager;

import biz.elevateglobal.timerManagerPackage.TimerManager.KeyedBroadcastProcessTimerManager.TestTimerManagerBroadcastProcess;
import biz.elevateglobal.messages.Instance;
import biz.elevateglobal.source.DataSource;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.watermark.Watermark;

import javax.annotation.Nullable;

public class ExampleTimerManagerInKeyedBroadcastProcess {
    final static boolean EVENT_TIME = true;

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(4);

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        DataStream<Instance> dataSource = env.addSource(new DataSource())
                .assignTimestampsAndWatermarks(new AssignerWithPunctuatedWatermarks<Instance>() {
                    @Nullable
                    @Override
                    public Watermark checkAndGetNextWatermark(Instance lastElement, long extractedTimestamp) {
                        return new Watermark(extractedTimestamp);
                    }

                    @Override
                    public long extractTimestamp(Instance element, long previousElementTimestamp) {
                        return element.getTimestamp();
                    }
                });

        DataStream<String> controlStream = env.addSource(new SourceFunction<String>() {
            @Override
            public void run(SourceContext<String> ctx) throws Exception {
                ctx.collect("ControlMessage");
            }

            @Override
            public void cancel() {
            }
        }).assignTimestampsAndWatermarks(new AssignerWithPunctuatedWatermarks<String>() {
            @Nullable
            @Override
            public Watermark checkAndGetNextWatermark(String lastElement, long extractedTimestamp) {
                return new Watermark(Long.MIN_VALUE);
            }

            @Override
            public long extractTimestamp(String element, long previousElementTimestamp) {
                return 0;
            }
        });

        MapStateDescriptor<Void, String> mapStateDescriptor = new MapStateDescriptor<Void, String>(
                "map",
                Types.VOID,
                Types.STRING
        );

        BroadcastStream<String> broadcastStream = controlStream.broadcast(
                mapStateDescriptor
        );

        dataSource
                .keyBy(Instance::getKey)
                .connect(broadcastStream)
                .process(new TestTimerManagerBroadcastProcess(EVENT_TIME, mapStateDescriptor))
                .print();

        env.execute("timer-manager");
    }
}
