package biz.elevateglobal.examples.ExamplesTimerManager;

import biz.elevateglobal.timerManagerPackage.TimerManager.KeyedCoProcessTimerManager.TestTimerManagerCoProcess;
import biz.elevateglobal.messages.Instance;
import biz.elevateglobal.source.DataSource;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.watermark.Watermark;

import javax.annotation.Nullable;

public class ExampleTimerManagerCoProcess {

    final static boolean EVENT_TIME = true;

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(4);

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        DataStream<Instance> dataSource = env.addSource(new DataSource())
                .assignTimestampsAndWatermarks(new AssignerWithPunctuatedWatermarks<Instance>() {
                    @Nullable
                    @Override
                    public Watermark checkAndGetNextWatermark(Instance lastElement, long extractedTimestamp) {
                        return new Watermark(extractedTimestamp);
                    }

                    @Override
                    public long extractTimestamp(Instance element, long previousElementTimestamp) {
                        return element.getTimestamp();
                    }
                });

        DataStream<String> secondDataStream = env.addSource(new SourceFunction<String>() {
            @Override
            public void run(SourceContext<String> ctx) throws Exception {
                ctx.collect("SecondStream");
            }

            @Override
            public void cancel() {
            }
        }).assignTimestampsAndWatermarks(new AssignerWithPunctuatedWatermarks<String>() {
            @Nullable
            @Override
            public Watermark checkAndGetNextWatermark(String lastElement, long extractedTimestamp) {
                return new Watermark(Long.MIN_VALUE);
            }

            @Override
            public long extractTimestamp(String element, long previousElementTimestamp) {
                return 0;
            }
        });


        dataSource.connect(secondDataStream)
                .keyBy(Instance::getKey, new KeySelector<String, String>() {
                    @Override
                    public String getKey(String value) throws Exception {
                        return value;
                    }
                })
                .process(new TestTimerManagerCoProcess(EVENT_TIME))
                .print();

        env.execute("timer-manager");
    }
}
