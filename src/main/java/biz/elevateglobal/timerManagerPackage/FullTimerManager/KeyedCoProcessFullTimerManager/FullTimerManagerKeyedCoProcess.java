package biz.elevateglobal.timerManagerPackage.FullTimerManager.KeyedCoProcessFullTimerManager;

import biz.elevateglobal.timerManagerPackage.FullTimerManager.Utills.TimerFactory;
import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerCoProcess;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.functions.co.KeyedCoProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of Timer Manager which can be used in keyed CoProcess function.
 * <p>
 * The Full Timer Manager have only one timer registered in Flink's timer service at any moment,
 * the others user-registered timers are fully managed by the timer manager.
 *
 * @param <E> Type of the output elements.
 */
public class FullTimerManagerKeyedCoProcess<E> implements Serializable {


    private static final int FIRST = 0;
    /**
     * true if time characteristics are setted to EventTime, false if setted to ProcessingTime.
     */
    private Boolean eventTime;

    /**
     * this is the officially registered timer in Apache Flink.
     */
    private ValueState<TimerInKeyedCoProcess<E>> registeredTimerState;

    /**
     * In this data structure are kept all other timers that ware added(pseudo registered) but will happend(fire) later
     * than the officially registered.
     */
    private ValueState<SortedMap<Long, List<TimerInKeyedCoProcess<E>>>> timersQueueState;

    /**
     * key-timer map. where key is the unique Id of the timer, generated at It's creation.
     */
    private ValueState<Map<Integer, TimerInKeyedCoProcess<E>>> timersUIDState;

    /**
     * this function is called from the Flink's onTimer function.
     *
     * @param ctx OnTimerContext in Flink's KeyedCoProcessFunction.
     * @param out Collector in Flink's KeyedCoProcessFunction.
     * @throws Exception
     */
    public void onTimer(KeyedCoProcessFunction.OnTimerContext ctx, Collector<E> out) throws Exception {
        fireTimer(ctx, out);
        //check if any unofficially registered timer needs to be fired and remove it
        fireExpiredTimers(ctx, out);

        reorderTimers(ctx);
    }

    /**
     * Fires the officially registered timer in Flink's timer service
     *
     * @param ctx OnTimerContext in Flink's KeyedCoProcessFunction.
     * @param out Collector in Flink's KeyedCoProcessFunction.
     * @throws IOException
     */
    private void fireTimer(KeyedCoProcessFunction.OnTimerContext ctx, Collector<E> out) throws IOException {
        if(registeredTimerState.value()==null){
            return;
        }
        registeredTimerState.value().fire(ctx, out);
        registeredTimerState.update(null);


    }


    /**
     * creates the Timer Manager and initialize the states.
     *
     * @param runtimeContext Flink's  runtime context.
     * @param eventTime      boolean variable, true if time characteristics are set to event-time, otherwise is false.
     * @throws IOException
     */
    public FullTimerManagerKeyedCoProcess(RuntimeContext runtimeContext, boolean eventTime) throws IOException {
        this.eventTime = eventTime;

        ValueStateDescriptor<TimerInKeyedCoProcess<E>> registeredTimerDesc = new ValueStateDescriptor<TimerInKeyedCoProcess<E>>(
                "reg-timer",
                TypeInformation.of(new TypeHint<TimerInKeyedCoProcess<E>>() {
                })
        );
        this.registeredTimerState = runtimeContext.getState(registeredTimerDesc);

        ValueStateDescriptor<SortedMap<Long, List<TimerInKeyedCoProcess<E>>>> timersForRegDesc =
                new ValueStateDescriptor<SortedMap<Long, List<TimerInKeyedCoProcess<E>>>>(
                        "timers-for-reg",
                        TypeInformation.of(
                                new TypeHint<SortedMap<Long, List<TimerInKeyedCoProcess<E>>>>() {
                                })
                );
        this.timersQueueState = runtimeContext.getState(timersForRegDesc);

        ValueStateDescriptor<Map<Integer, TimerInKeyedCoProcess<E>>> addedTimersDescription =
                new ValueStateDescriptor<Map<Integer, TimerInKeyedCoProcess<E>>>(
                        "added-timers",
                        TypeInformation.of(
                                new TypeHint<Map<Integer, TimerInKeyedCoProcess<E>>>() {
                                })
                );
        this.timersUIDState = runtimeContext.getState(addedTimersDescription);
    }

    /**
     * initialize the states in the timer manager
     *
     * @throws IOException
     */
    private void initializeStates() throws IOException {
        //in case of recovering after failure or in case the states are already initialized, do nothing, otherwise initialize the states
        if (this.timersUIDState.value() != null)
            return;
        this.timersQueueState.update(new TreeMap<>(Comparator.comparing(Long::longValue)));
        this.timersUIDState.update(new HashMap<>());

    }

    /**
     * register a timer at time time with callback onTimerFunct (which will be executed when the timer fires).
     *
     * @param triggeringTime   time at which the timer will be registered (if the time is not in the past).
     * @param onTimerCallback- Implemented IOnTimerCoProcess (callback function).
     * @param context          Context in Flink's KeyedCoProcessFunction.
     * @return unique ID of the registered timer, only with this ID this timer can be deleted later.If the triggering time of the timer
     * is not valid -1 is returned
     * @throws IOException
     */
    public int registerTimer(long triggeringTime, IOnTimerCoProcess onTimerCallback, KeyedCoProcessFunction.Context context) throws IOException {

        if (!this.isValidTriggeringTime(triggeringTime, context))
            return -1;

        TimerInKeyedCoProcess<E> newTimer = TimerFactory.<E>createTimer(triggeringTime, onTimerCallback);

        initializeStates();

        addTimer(newTimer, context);

        reorderTimers(context);

        trackTimer(newTimer);

        return newTimer.getUniqueId();
    }

    /**
     * Adds the new timer (newTimer) to the pseudo registered and then does reordering of the timers to make sure
     * that the the timer that will fire first is officially registered in Flink's timer service
     *
     * @param newTimer the timer that will be registered
     * @param context  Context in Flink's KeyedCoProcessFunction
     * @throws IOException
     */
    private void addTimer(TimerInKeyedCoProcess<E> newTimer, KeyedCoProcessFunction.Context context) throws IOException {
        SortedMap<Long, List<TimerInKeyedCoProcess<E>>> timersForReg = timersQueueState.value();

        timersForReg.computeIfPresent(newTimer.getTime(), (timer, queue) -> {
            queue.add(newTimer);
            return queue;
        });

        timersForReg.computeIfAbsent(newTimer.getTime(), (timer) -> {
            ArrayList<TimerInKeyedCoProcess<E>> l = new ArrayList<>();
            l.add(newTimer);
            return l;
        });

        timersQueueState.update(timersForReg);
    }

    /**
     * unregister the timer from Apache Flink's timer service.
     *
     * @param timerToBeUnregistered the timer that needs to be unregistered.
     * @param context               Context in Flink's KeyedCoProcessFunction.
     */
    private void unregisterTimer(TimerInKeyedCoProcess<E> timerToBeUnregistered, KeyedCoProcessFunction.Context context) {
        if (this.eventTime)
            context.timerService().deleteEventTimeTimer(timerToBeUnregistered.getTime());
        context.timerService().deleteEventTimeTimer(timerToBeUnregistered.getTime());
    }

    /**
     * deletes the timer passed as argument from the pseudo and officially registered, but do not remove it's track (It's Id).
     *
     * @param timerToBeDeleted timer that will to be deleted.
     * @param context          Context in Flink's KeyedCoProcessFunction.
     * @throws IOException
     */
    private void cleanStates(TimerInKeyedCoProcess<E> timerToBeDeleted, KeyedCoProcessFunction.Context context) throws IOException {
        boolean deleted = deleteOfficialTimer(timerToBeDeleted, context);

        if (deleted)
            return;

        SortedMap<Long, List<TimerInKeyedCoProcess<E>>> timersForReg = timersQueueState.value();
        this.removeTimerFromDataStructure(timerToBeDeleted, timersForReg);
    }

    /**
     * checks if the timer passed as an argument is officially registered in Apache Flink's timer service.
     * If yes then unregister it, otherwise does nothing
     *
     * @param timerToBeDeleted the timer that needs to be unregistered if it is officially registered in Apache Flink's timer service
     * @param context          Context in Flink's KeyedCoProcessFunction
     * @return
     * @throws IOException
     */
    private boolean deleteOfficialTimer(TimerInKeyedCoProcess<E> timerToBeDeleted, KeyedCoProcessFunction.Context context) throws IOException {
        TimerInKeyedCoProcess<E> registeredTimer = registeredTimerState.value();
        if (registeredTimer != null && registeredTimer.equals(timerToBeDeleted)) {
            registeredTimerState.update(null);
            unregisterTimer(timerToBeDeleted, context);
            return true;
        }
        return false;
    }

    /**
     * removes the timer timerToBeRemoved from the data structure for pseudo registered timers.
     *
     * @param timerToBeRemoved the timer that will be removed from data structure for pseudo registered timers.
     * @param timersForReg     data structure where the pseudo registered timers are stored.
     * @throws IOException
     */
    private void removeTimerFromDataStructure(TimerInKeyedCoProcess<E> timerToBeRemoved, SortedMap<Long, List<TimerInKeyedCoProcess<E>>> timersForReg) throws IOException {
        List<TimerInKeyedCoProcess<E>> queue = timersForReg.getOrDefault(timerToBeRemoved.getTime(), null);
        if (queue == null)
            return;
        queue.remove(timerToBeRemoved);

        if (queue.size() == 0)
            timersForReg.remove(timerToBeRemoved.getTime());
        timersQueueState.update(timersForReg);
    }

    /**
     * deletes everything about the timer specified with timerToDeleteID (It's ID), if it exists.
     *
     * @param timerToDeleteID the ID of the timer that you want delete.
     * @param context         Contex in Flink's KeyedCoProcessFunction.
     * @throws IOException
     */
    public void unregisterTimer(int timerToDeleteID, KeyedCoProcessFunction.Context context) throws IOException {
        if (!isTimerRegistered(timerToDeleteID)) {
            return;
        }

        if (registeredTimerState.value() != null && registeredTimerState.value().getUniqueId() == timerToDeleteID) {
            if (this.eventTime) {
                context.timerService().deleteEventTimeTimer(timerToDeleteID);
            } else {
                context.timerService().deleteProcessingTimeTimer(timerToDeleteID);
            }

        }

        Map<Integer, TimerInKeyedCoProcess<E>> timersUID = this.timersUIDState.value();
        TimerInKeyedCoProcess<E> timerToBeDeleted = timersUID.get(timerToDeleteID);

        untrackTimer(timerToBeDeleted);

        cleanStates(timerToBeDeleted, context);

        reorderTimers(context);
    }

    /**
     * checks if the there is registered timer with the specified ID.
     *
     * @param timerToDeleteID the Id of the timer.
     * @return true if the timer with the specified Id exists otherwise returns false.
     * @throws IOException
     */
    private boolean isTimerRegistered(int timerToDeleteID) throws IOException {
        Map<Integer, TimerInKeyedCoProcess<E>> timersUID = this.timersUIDState.value();
        return timersUID.containsKey(timerToDeleteID);
    }

    /**
     * removes the timer passed as an argument from the tracked timers(timersUIDState).
     *
     * @param timerToBeDeleted the timer which will be removed from timersUIDState
     * @throws IOException
     */
    private void untrackTimer(TimerInKeyedCoProcess<E> timerToBeDeleted) throws IOException {
        Map<Integer, TimerInKeyedCoProcess<E>> timersUID = this.timersUIDState.value();

        timersUID.remove(timerToBeDeleted.getUniqueId());

        this.timersUIDState.update(timersUID);
    }

    /**
     * maps the ID and the corresponding timer in the map addedTimers.
     *
     * @param timerToTrack - the timer that will be tracked with it's id.
     * @throws IOException
     */
    private void trackTimer(TimerInKeyedCoProcess<E> timerToTrack) throws IOException {
        Map<Integer, TimerInKeyedCoProcess<E>> addedTimersMap = timersUIDState.value();
        addedTimersMap.put(timerToTrack.getUniqueId(), timerToTrack);
        timersUIDState.update(addedTimersMap);
    }

    /**
     * register the timer passed as an argument using Apache Flink's timer service.
     *
     * @param timerToRegisterInFlink the timer that will be officially registered.
     * @param context                Context in Flink's KeyedCoProcessFunction.
     * @throws IOException
     */
    private void registerTimer(TimerInKeyedCoProcess<E> timerToRegisterInFlink, KeyedCoProcessFunction.Context context) throws IOException {
        if (this.eventTime) {
            context.timerService().registerEventTimeTimer(timerToRegisterInFlink.getTime());
        } else {
            context.timerService().registerProcessingTimeTimer(timerToRegisterInFlink.getTime());
        }
        registeredTimerState.update(timerToRegisterInFlink);

    }

    /**
     * checks if any unofficially registered (not registered in Flink's timer service) timer needs to be fired
     * if yes, then call call artificialOnTimer with the timer as argument.
     *
     * @param context OnTimerContext in Flink's KeyedCoProcessFunction.
     * @param out     Collector in Flink's KeyedCoProcessFunction.
     * @throws IOException
     */
    private void fireExpiredTimers(KeyedCoProcessFunction.OnTimerContext context, Collector<E> out) throws IOException {

        long currentTime = this.getCurrentTime(context);
        SortedMap<Long, List<TimerInKeyedCoProcess<E>>> timersQueue = this.timersQueueState.value();

        if (timersQueue.size() == 0)
            return;

        timersQueue.keySet().stream().filter(triggeringTime -> triggeringTime < currentTime).map(timersQueue::get)
                .forEach(timersQueueForFiring -> this.fireAllTimers(timersQueueForFiring, context, out));

        List<Long> expiredTriggeringTimes = timersQueue.keySet().stream().filter(triggerTime -> triggerTime < currentTime)
                .collect(Collectors.toList());

        expiredTriggeringTimes.stream().forEach(tirggeringTime -> timersQueue.remove(tirggeringTime));

        this.timersQueueState.update(timersQueue);

    }

    /**
     * fires all the timers that are in the passed list timersQueueForFiring
     *
     * @param timersQueueForFiring a list containing timers that will are triggered and will be fired.
     * @param context              OnTimerContext in Flink's KeyedCoProcessFunction.
     * @param out                  Context in Flink's KeyedCoProcessFunction.
     */
    private void fireAllTimers(List<TimerInKeyedCoProcess<E>> timersQueueForFiring, KeyedCoProcessFunction.OnTimerContext context,
                               Collector<E> out) {
        timersQueueForFiring.forEach(timerForFiring -> {
            try {
                timerForFiring.fire(context, out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * The main logic of the timer management is done here
     * Checks if there is unofficially registered timer that should ne fired earlier than the officially registered
     * if yes then swaps them, otherwise do nothing.
     *
     * @param context Context in Flink's KeyedCoProcessFunction.
     * @throws IOException
     */
    private void reorderTimers(KeyedCoProcessFunction.Context context) throws IOException {
        //timers that are pseudo registered (registered in the timer menager)
        SortedMap<Long, List<TimerInKeyedCoProcess<E>>> timersQueue = this.timersQueueState.value();
        if (timersQueue.size() == 0)
            return;
        //timer that is officially registered in Flink's timer service
        TimerInKeyedCoProcess<E> officiallyRegisteredTimer = registeredTimerState.value();

        //the timer that will fire first from the unofficially registered timers
        TimerInKeyedCoProcess<E> nextTimerCandidate = timersQueue.get(timersQueue.firstKey()).get(FIRST);

        if (officiallyRegisteredTimer != null && officiallyRegisteredTimer.getTime() < nextTimerCandidate.getTime())
            //there is already registered timer and the candidate for nextTimer will happen after the officially registered
            return;
        else if (officiallyRegisteredTimer != null && officiallyRegisteredTimer.getTime() > nextTimerCandidate.getTime()) {
            //the candidate for nextTimer needs to be fired before the officially registered
            //unregister the officially registered and add to the queued
            this.moveOfficialTimerToPseudoRegistered(officiallyRegisteredTimer, context, timersQueue);
            officiallyRegisteredTimer = null;
        }
        //at the end do the check if there is an officially registered timer at all
        //if not register the next candidate(the one with smallest time for firing)
        if (officiallyRegisteredTimer == null) {
            //this will delete timer from where it currently exists - the pseudo registered timers(timersForRegistering)
            this.cleanStates(nextTimerCandidate, context);
            //register the timer that should fire first
            this.registerTimer(nextTimerCandidate, context);
        }
        this.timersQueueState.update(timersQueue);
    }

    /**
     * deletes the officially registered timer and adds it to pseudo registered
     *
     * @param officialTimer timer that is registered in Flink's timer service.
     * @param context       Context in Flink's KeyedCoProcessFunction.
     * @param timersQueue   Timers that are pseudo registered (not officially registered in Apache Flink timer service)
     * @throws IOException
     */
    private void moveOfficialTimerToPseudoRegistered(TimerInKeyedCoProcess<E> officialTimer, KeyedCoProcessFunction.Context context,
                                                     SortedMap<Long, List<TimerInKeyedCoProcess<E>>> timersQueue) throws IOException {

        //unregister the officialTimer
        this.cleanStates(officialTimer, context);
        //put the previously officially registered timer in the the tree for timers waiting to be registered
        timersQueue.computeIfAbsent(officialTimer.getTime(), (key) -> {
            List<TimerInKeyedCoProcess<E>> queue = new ArrayList<>();
            return queue;
        });
        timersQueue.get(officialTimer.getTime()).add(officialTimer);
    }

    /**
     * returns current watermark if time charachteristics are set to event time
     * otherwise returns current processing time.
     *
     * @param context Context in Flink's KeyedCoProcessFunction.
     * @return
     */
    private long getCurrentTime(KeyedCoProcessFunction.Context context) {
        if (this.eventTime)
            return context.timerService().currentWatermark();
        return context.timerService().currentProcessingTime();
    }

    /**
     * checks if the triggeringTime is valid
     *
     * @param triggeringTime the triggering time that needs to be checked
     * @param context        Context in Flink's KeyedCoProcessFunction.
     * @return
     */
    private boolean isValidTriggeringTime(long triggeringTime, KeyedCoProcessFunction.Context context) {
        if (triggeringTime < 0) {
            return Boolean.FALSE;
        }
        if (triggeringTime < this.getCurrentTime(context)) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

}
