package biz.elevateglobal.timerManagerPackage.FullTimerManager.KeyedCoProcessFullTimerManager;

import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerCoProcess;
import org.apache.flink.streaming.api.functions.co.KeyedCoProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;

/**
 * This class represents a timer which is used in KeyedCoProcessFunction.
 *
 * @param <E> - Type of the output elements.
 */
public class TimerInKeyedCoProcess<E> {

    /**
     * the time at which this timer should be fired.
     */
    private long time;

    /**
     * the callback which defines the behaviour which should be executed when this timer fires.
     */
    private IOnTimerCoProcess<E> iOnTimer;

    /**
     * unique ID of the timer, this ID can be used for deleting the registered timer.
     */
    private int uniqueId;

    public TimerInKeyedCoProcess(long time, IOnTimerCoProcess<E> iOnTimer) {
        this.time = time;
        this.iOnTimer = iOnTimer;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public IOnTimerCoProcess<E> getiOnTimer() {
        return iOnTimer;
    }

    public void setiOnTimer(IOnTimerCoProcess<E> iOnTimer) {
        this.iOnTimer = iOnTimer;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimerInKeyedCoProcess<?> timer = (TimerInKeyedCoProcess<?>) o;
        return time == timer.time &&
                uniqueId == timer.uniqueId;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    /**
     * execute the defined callback in this timer.
     *
     * @param context OnTimerContext in Flink's KeyedCoProcessFunction
     * @param out     Collector in Flink's KeyedCoProcessFunction.
     * @throws IOException
     */
    public void fire(KeyedCoProcessFunction.OnTimerContext context, Collector<E> out) throws IOException {
        this.iOnTimer.onTimerFunction(this.time, context, out);
    }
}
