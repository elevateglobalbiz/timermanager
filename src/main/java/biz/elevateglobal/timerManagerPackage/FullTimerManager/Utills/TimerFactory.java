package biz.elevateglobal.timerManagerPackage.FullTimerManager.Utills;

import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerKeyedBroadcast;
import biz.elevateglobal.timerManagerPackage.FullTimerManager.KeyedBroadcastProcessFullTimerManager.TimerInKeyedBroadcastProcess;
import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerCoProcess;
import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerKeyedProcessCallback;
import biz.elevateglobal.timerManagerPackage.FullTimerManager.KeyedProcessFullTimerManager.TimerInKeyedProcess;
import biz.elevateglobal.timerManagerPackage.FullTimerManager.KeyedCoProcessFullTimerManager.TimerInKeyedCoProcess;

import static biz.elevateglobal.timerManagerPackage.FullTimerManager.Utills.GenerateTimerUID.generateTimerUID;


public class TimerFactory {


    public static <E> TimerInKeyedProcess<E> createTimer(long triggeringTime, IOnTimerKeyedProcessCallback<E> onTimerCallback) {
        TimerInKeyedProcess<E> newTimer = new TimerInKeyedProcess<>(triggeringTime, onTimerCallback);
        newTimer.setUniqueId(generateTimerUID(triggeringTime));
        return newTimer;
    }

    public static <E> TimerInKeyedCoProcess<E> createTimer(long triggeringTime, IOnTimerCoProcess<E> onTimerCallback) {
        TimerInKeyedCoProcess<E> newTimer = new TimerInKeyedCoProcess<>(triggeringTime, onTimerCallback);
        newTimer.setUniqueId(generateTimerUID(triggeringTime));
        return newTimer;
    }

    public static <E> TimerInKeyedBroadcastProcess<E> createTimer(long triggeringTime, IOnTimerKeyedBroadcast<E> onTimerCallback) {
        TimerInKeyedBroadcastProcess<E> newTimer = new TimerInKeyedBroadcastProcess<>(triggeringTime, onTimerCallback);
        newTimer.setUniqueId(generateTimerUID(triggeringTime));
        return newTimer;
    }
}
