package biz.elevateglobal.timerManagerPackage.FullTimerManager.Utills;

import java.util.Objects;

public class GenerateTimerUID {

    public static int generateTimerUID(long triggeringTime) {
        return Objects.hash(triggeringTime, System.currentTimeMillis(), Math.random() * 100000);
    }
}
