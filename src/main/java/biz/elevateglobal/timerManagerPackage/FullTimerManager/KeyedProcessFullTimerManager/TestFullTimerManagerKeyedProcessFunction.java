package biz.elevateglobal.timerManagerPackage.FullTimerManager.KeyedProcessFullTimerManager;

import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerKeyedProcessCallback;
import biz.elevateglobal.messages.Instance;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Example of using and utilizing Timer Manager(TimerManagerKeyedProcess) in keyed process function
 */
public class TestFullTimerManagerKeyedProcessFunction extends KeyedProcessFunction<String, Instance, String> {
    /**
     * boolean variable about the setted time characteristics.
     */
    boolean eventTime;

    /**
     * a reference to the Timer Manager
     */
    FullTimerManagerKeyedProcess<String> timerManager;

    /**
     * keep evidence of the registered timers and their IDs
     */
    ValueState<Map<Long, Integer>> regTimersUID;

    public TestFullTimerManagerKeyedProcessFunction(boolean eventTime) throws IOException {
        this.eventTime = eventTime;
    }

    // In order to instantiate the states in the timer manager, runtumeContext is passed in it’s constructor.
    @Override
    public void open(Configuration parameters) throws Exception {
        // and boolean value about the time charachteristic to its constructor
        timerManager = new FullTimerManagerKeyedProcess<>(getRuntimeContext(), eventTime);

        ValueStateDescriptor<Map<Long, Integer>> registeredTimersUIDDescriptor = new ValueStateDescriptor<Map<Long, Integer>>(
                "reg-timers-uid",
                TypeInformation.of(new TypeHint<Map<Long, Integer>>() {
                })
        );

        regTimersUID = getRuntimeContext().getState(registeredTimersUIDDescriptor);
    }


    // This is the official Apache Flink's onTimer function.
    // The onTimer function of the Timer Manager must be call here for proper functioning of the Timer Manager.
    @Override
    public void onTimer(long timestamp, OnTimerContext ctx, Collector<String> out) throws Exception {
        timerManager.onTimer(ctx, out);
    }

    private void initializeStates() throws IOException {
        //in case of recovering after failure or the states are already initialized do nothing, otherwise initialize the states
        if (regTimersUID.value() != null)
            return;
        regTimersUID.update(new HashMap<>());

    }

    //The Timer Manager’s API for registering and deleting timers is used here
    //when registering, the addTimer(...) from TimerManager returns unique Id of the registered timer
    //this id needs to be saved if you like to delete the corresponding timer later
    @Override
    public void processElement(Instance value, Context ctx, Collector<String> out) throws Exception {

        this.initializeStates();

        long triggeringTime;

        long currentWatermark = ctx.timerService().currentWatermark();

        if (value.getTimestamp() % 2 == 0) {
            triggeringTime = currentWatermark + 3;
        } else {
            triggeringTime = currentWatermark + 10;
        }

        IOnTimerKeyedProcessCallback<String> iOnTimerKeyedProcessCallbackFunct = (timestamp, context, collector) ->
                collector.collect("Timer registered at " + (timestamp) + " from instance with timestamp "
                        + value.getTimestamp() + " has fired");

        int newTimerUniqueId = timerManager.registerTimer(triggeringTime, iOnTimerKeyedProcessCallbackFunct, ctx);
        if (newTimerUniqueId == -1)
            return;

        System.out.println("Instance with timestamp " + value.getTimestamp() + " and key=" + ctx.getCurrentKey() + "\n"
                + "Current watermark " + currentWatermark + "\n"
                + "Registered timer on " + (triggeringTime));

        Map<Long, Integer> regTimersUID = this.regTimersUID.value();

        regTimersUID.put(value.getTimestamp(), newTimerUniqueId);

        this.regTimersUID.update(regTimersUID);
    }
}
