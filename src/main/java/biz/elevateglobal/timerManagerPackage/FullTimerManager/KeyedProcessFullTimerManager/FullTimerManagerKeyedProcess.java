package biz.elevateglobal.timerManagerPackage.FullTimerManager.KeyedProcessFullTimerManager;

import biz.elevateglobal.timerManagerPackage.FullTimerManager.Utills.TimerFactory;
import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerKeyedProcessCallback;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of Timer Manager which can be used in keyed Process function.
 *
 * The Full Timer Manager have only one timer registered in Flink's timer service at any moment,
 * the others user-registered timers are fully managed by the timer manager.
 *
 * @param <E> Type of the output elements.
 */
public class FullTimerManagerKeyedProcess<E> implements Serializable {


    private static final int FIRST = 0;
    /**
     * true if time characteristics are setted to EventTime, false if setted to ProcessingTime.
     */
    private Boolean eventTime;

    /**
     * this is the officially registered timer in Apache Flink.
     */
    private ValueState<TimerInKeyedProcess<E>> registeredTimerState;

    /**
     * In this data structure are kept all other timers that ware added(pseudo registered) but will happend(fire) later
     * than the officially registered.
     */
    private ValueState<SortedMap<Long, List<TimerInKeyedProcess<E>>>> timersQueueState;

    /**
     * key-timer map. where key is the unique Id of the timer, generated at It's creation.
     */
    private ValueState<Map<Integer, TimerInKeyedProcess<E>>> timersUIDState;

    /**
     * this function is called from the Flink's onTimer function.
     *
     * @param ctx OnTimerContext in Flink's KeyedProcessFunction.
     * @param out Collector in Flink's KeyedProcessFunction.
     * @throws Exception
     */
    public void onTimer(KeyedProcessFunction.OnTimerContext ctx, Collector<E> out) throws Exception {
        fireTimer(ctx, out);
        //check if any unofficially registered timer needs to be fired and remove it
        fireExpiredTimers(ctx, out);

        reorderTimers(ctx);
    }

    /**
     * Fires the officially registered timer in Flink's timer service
     *
     * @param ctx OnTimerContext in Flink's KeyedProcessFunction.
     * @param out Collector in Flink's KeyedProcessFunction.
     * @throws IOException
     */
    private void fireTimer(KeyedProcessFunction.OnTimerContext ctx, Collector<E> out) throws IOException {
        registeredTimerState.value().fire(ctx, out);
        registeredTimerState.update(null);
    }


    /**
     * creates the Timer Manager and initialize the states.
     *
     * @param runtimeContext Flink's runtime context.
     * @param eventTime      boolean variable, true if time characteristics are set to event-time, otherwise is false.
     * @throws IOException
     */
    public FullTimerManagerKeyedProcess(RuntimeContext runtimeContext, boolean eventTime) throws IOException {
        this.eventTime = eventTime;
        ValueStateDescriptor<TimerInKeyedProcess<E>> registeredTimerDesc = new ValueStateDescriptor<TimerInKeyedProcess<E>>(
                "reg-timer",
                TypeInformation.of(new TypeHint<TimerInKeyedProcess<E>>() {
                })
        );
        this.registeredTimerState = runtimeContext.getState(registeredTimerDesc);

        ValueStateDescriptor<SortedMap<Long, List<TimerInKeyedProcess<E>>>> timersForRegDesc =
                new ValueStateDescriptor<SortedMap<Long, List<TimerInKeyedProcess<E>>>>(
                        "timers-for-reg",
                        TypeInformation.of(
                                new TypeHint<SortedMap<Long, List<TimerInKeyedProcess<E>>>>() {
                                })
                );
        this.timersQueueState = runtimeContext.getState(timersForRegDesc);

        ValueStateDescriptor<Map<Integer, TimerInKeyedProcess<E>>> addedTimersDescription =
                new ValueStateDescriptor<Map<Integer, TimerInKeyedProcess<E>>>(
                        "added-timers",
                        TypeInformation.of(
                                new TypeHint<Map<Integer, TimerInKeyedProcess<E>>>() {
                                })
                );
        this.timersUIDState = runtimeContext.getState(addedTimersDescription);

    }

    /**
     * initialize the states in the timer manager
     *
     * @throws IOException
     */
    private void initializeStates() throws IOException {
        //in case of recovering after failure or the states are already initialized do nothing, otherwise initialize the states
        if (timersUIDState.value() != null)
            return;
        timersQueueState.update(new TreeMap<>(Comparator.comparing(Long::longValue)));
        timersUIDState.update(new HashMap<>());

    }

    /**
     * register a timer at time time with callback onTimerFunct (which will be executed when the timer fires).
     *
     * @param triggeringTime  time at which the timer will be registered (if the time is not in the past).
     * @param onTimerCallback Implemented IOnTimerKeyedProcess (callback function).
     * @param context         Context in Flink's KeyedProcessFunction.
     * @return unique ID of the registered timer, only with this ID this timer can be deleted later. If the triggering time of the timer
     *  is not valid -1 is returned
     * @throws IOException
     */
    public int registerTimer(long triggeringTime, IOnTimerKeyedProcessCallback onTimerCallback, KeyedProcessFunction.Context context) throws IOException {

        if (!this.isValidTriggeringTime(triggeringTime, context))
            return -1;

        initializeStates();

        TimerInKeyedProcess<E> newTimer = TimerFactory.<E>createTimer(triggeringTime, onTimerCallback);

        addTimer(newTimer, context);

        reorderTimers(context);

        trackTimer(newTimer);

        return newTimer.getUID();
    }

    /**
     * Adds the new timer (newTimer) to the pseudo registered and then does reordering of the timers to make sure
     * that the the timer that will fire first is officially registered in Flink's timer service
     *
     * @param newTimer the timer that will be registered
     * @param context  Context in Flink's KeyedProcessFunction
     * @throws IOException
     */
    private void addTimer(TimerInKeyedProcess<E> newTimer, KeyedProcessFunction.Context context) throws IOException {
        SortedMap<Long, List<TimerInKeyedProcess<E>>> timersForReg = timersQueueState.value();

        timersForReg.computeIfPresent(newTimer.getTime(), (timer, queue) -> {
            queue.add(newTimer);
            return queue;
        });

        timersForReg.computeIfAbsent(newTimer.getTime(), (timer) -> {
            ArrayList<TimerInKeyedProcess<E>> l = new ArrayList<>();
            l.add(newTimer);
            return l;
        });

        timersQueueState.update(timersForReg);
    }


    /**
     * unregister the timer from Apache Flink's timer service.
     *
     * @param timerToBeUnregistered the timer that needs to be unregistered.
     * @param context               Context in Flink's KeyedProcessFunction.
     */
    private void unregisterTimer(TimerInKeyedProcess<E> timerToBeUnregistered, KeyedProcessFunction.Context context) {
        if (this.eventTime) {
            context.timerService().deleteEventTimeTimer(timerToBeUnregistered.getTime());
            return;
        }
        context.timerService().deleteProcessingTimeTimer(timerToBeUnregistered.getTime());
    }


    /**
     * deletes the timer passed as argument from the pseudo and officially registered, but do not remove it's track (It's Id).
     *
     * @param timerToBeDeleted timer that will to be deleted.
     * @param context          Context in Flink's KeyedProcessFunction.
     * @throws IOException
     */
    private void cleanStates(TimerInKeyedProcess<E> timerToBeDeleted, KeyedProcessFunction.Context context) throws IOException {
        boolean deleted = deleteOfficialTimer(timerToBeDeleted, context);
        if (deleted)
            return;

        SortedMap<Long, List<TimerInKeyedProcess<E>>> timersQueue = timersQueueState.value();
        this.removeTimerFromDataStructure(timerToBeDeleted, timersQueue);
    }

    /**
     * checks if the timer passed as an argument is officially registered in Apache Flink's timer service.
     * If yes then unregister it, otherwise does nothing
     *
     * @param timerToBeDeleted the timer that needs to be unregistered if it is officially registered in Apache Flink's timer service
     * @param context          Context in Flink's KeyedProcessFunction
     * @return
     * @throws IOException
     */
    private boolean deleteOfficialTimer(TimerInKeyedProcess<E> timerToBeDeleted, KeyedProcessFunction.Context context) throws IOException {
        TimerInKeyedProcess<E> registeredTimer = registeredTimerState.value();
        if (registeredTimer != null && registeredTimer.equals(timerToBeDeleted)) {
            registeredTimerState.update(null);
            unregisterTimer(timerToBeDeleted, context);
            return true;
        }
        return false;
    }

    /**
     * removes the timer timerToBeRemoved from the data structure for pseudo registered timers
     *
     * @param timerToBeRemoved the timer that will be removed from data structure for pseudo registered timers.
     * @param timersForReg     data structure where the pseudo registered timers are stored.
     * @throws IOException
     */
    private void removeTimerFromDataStructure(TimerInKeyedProcess<E> timerToBeRemoved, SortedMap<Long, List<TimerInKeyedProcess<E>>> timersForReg) throws IOException {
        List<TimerInKeyedProcess<E>> queue = timersForReg.getOrDefault(timerToBeRemoved.getTime(), null);
        if (queue == null)
            return;
        queue.remove(timerToBeRemoved);

        if (queue.size() == 0)
            timersForReg.remove(timerToBeRemoved.getTime());
        timersQueueState.update(timersForReg);
    }

    /**
     * deletes everything about the timer specified with timerToDeleteID (It's ID), if it exists.
     *
     * @param timerToDeleteID the ID of the timer that you want delete.
     * @param context         Context in Flink's KeyedProcessFunction.
     * @throws IOException
     */
    public void unregisterTimer(int timerToDeleteID, KeyedProcessFunction.Context context) throws IOException {
        if (!isTimerRegistered(timerToDeleteID)) {
            return;
        }

        Map<Integer, TimerInKeyedProcess<E>> timersUID = this.timersUIDState.value();
        TimerInKeyedProcess<E> timerToBeDeleted = timersUID.get(timerToDeleteID);

        untrackTimer(timerToBeDeleted);

        cleanStates(timerToBeDeleted, context);

        reorderTimers(context);
    }

    /**
     * checks if the there is registered timer with the specified ID.
     *
     * @param timerToDeleteID the Id of the timer.
     * @return true if the timer with the specified Id exists otherwise returns false.
     * @throws IOException
     */
    private boolean isTimerRegistered(int timerToDeleteID) throws IOException {
        Map<Integer, TimerInKeyedProcess<E>> timersUID = this.timersUIDState.value();
        return timersUID.containsKey(timerToDeleteID);
    }

    /**
     * removes the timer passed as an argument from the tracked timers(timersUIDState).
     *
     * @param timerToBeDeleted the timer which will be removed from timersUIDState.
     * @throws IOException
     */
    private void untrackTimer(TimerInKeyedProcess<E> timerToBeDeleted) throws IOException {
        Map<Integer, TimerInKeyedProcess<E>> timersUID = this.timersUIDState.value();

        timersUID.remove(timerToBeDeleted.getUID());

        this.timersUIDState.update(timersUID);
    }

    /**
     * maps the ID and the corresponding timer in the map addedTimers.
     *
     * @param timerToTrack the timer that will be tracked with it's id.
     * @throws IOException
     */
    private void trackTimer(TimerInKeyedProcess<E> timerToTrack) throws IOException {
        Map<Integer, TimerInKeyedProcess<E>> timersUID = this.timersUIDState.value();
        timersUID.put(timerToTrack.getUID(), timerToTrack);
        this.timersUIDState.update(timersUID);
    }

    /**
     * register the timer passed as an argument using Apache Flink's timer service.
     *
     * @param timerToRegisterInFlink the timer that will be officially registered.
     * @param context                Context in Flink's KeyedProcessFunction.
     * @throws IOException
     */
    private void registerTimer(TimerInKeyedProcess<E> timerToRegisterInFlink, KeyedProcessFunction.Context context) throws IOException {
        if (this.eventTime) {
            context.timerService().registerEventTimeTimer(timerToRegisterInFlink.getTime());
        } else {
            context.timerService().registerProcessingTimeTimer(timerToRegisterInFlink.getTime());
        }
        registeredTimerState.update(timerToRegisterInFlink);
    }


    /**
     * checks if any unofficially registered (not registered in Flink's timer service) timer needs to be fired
     * if yes, then call call artificialOnTimer with the timer as argument.
     *
     * @param context OnTimerContext in Flink's KeyedProcessFunction.
     * @param out     Collector in Flink's KeyedProcessFunction.
     * @throws IOException
     */
    private void fireExpiredTimers(KeyedProcessFunction.OnTimerContext context, Collector<E> out) throws IOException {

        long currentTime = this.getCurrentTime(context);
        SortedMap<Long, List<TimerInKeyedProcess<E>>> timersQueue = this.timersQueueState.value();

        if (timersQueue.size() == 0)
            return;

        timersQueue.keySet().stream().filter(triggeringTime -> triggeringTime < currentTime).map(timersQueue::get)
                .forEach(timersQueueForFiring -> this.fireAllTimers(timersQueueForFiring, context, out));

        List<Long> expiredTriggeringTimes = timersQueue.keySet().stream().filter(triggerTime -> triggerTime < currentTime)
                .collect(Collectors.toList());

        expiredTriggeringTimes.stream().forEach(triggeringTime -> timersQueue.remove(triggeringTime));

        this.timersQueueState.update(timersQueue);
    }

    /**
     * fires all the timers that are in the passed list timersQueue
     *
     * @param timersQueue a list containing timers that will are triggered and will be fired
     * @param context     OnTimerContext in Flink's KeyedProcessFunction.
     * @param out         Collector in Flink's KeyedProcessFunction.
     */
    private void fireAllTimers(List<TimerInKeyedProcess<E>> timersQueue, KeyedProcessFunction.OnTimerContext context,
                               Collector<E> out) {
        timersQueue.forEach(timerForFiring -> {
            try {
                timerForFiring.fire(context, out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * The main logic of the timer management is done here
     * Checks if there is unofficially registered timer that should ne fired earlier than the officially registered
     * if yes then swaps them, otherwise do nothing.
     *
     * @param context Context in Flink's KeyedProcessFunction.
     * @throws IOException
     */
    private void reorderTimers(KeyedProcessFunction.Context context) throws IOException {
        //timers that are pseudo registered (registered in the timer menager)
        SortedMap<Long, List<TimerInKeyedProcess<E>>> timersQueue = this.timersQueueState.value();
        if (timersQueue.isEmpty())
            return;
        //timer that is officially registered in Flink's timer service
        TimerInKeyedProcess<E> officiallyRegisteredTimer = registeredTimerState.value();

        //the timer that will fire first from the unofficially registered timers
        TimerInKeyedProcess<E> nextTimerCandidate = timersQueue.get(timersQueue.firstKey()).get(FIRST);

        if (officiallyRegisteredTimer != null && officiallyRegisteredTimer.getTime() < nextTimerCandidate.getTime())
            //there is already registered timer and the candidate for nextTimer will happen after the officially registered
            return;
        else if (officiallyRegisteredTimer != null && officiallyRegisteredTimer.getTime() > nextTimerCandidate.getTime()) {
            //the candidate for nextTimer needs to be fired before the officially registered
            //unregister the officially registered and add to the queued
            this.moveOfficialTimerToPseudoRegistered(officiallyRegisteredTimer, context, timersQueue);
            officiallyRegisteredTimer = null;
        }
        //at the end do the check if there is an officially registered timer at all
        //if not register the next candidate(the one with smallest time for firing)
        if (officiallyRegisteredTimer == null) {
            //this will delete timer from where it currently exists - the pseudo registered timers(timersForRegistering)
            this.cleanStates(nextTimerCandidate, context);
            //register the timer that should fire first
            this.registerTimer(nextTimerCandidate, context);
        }
        this.timersQueueState.update(timersQueue);
    }


    /**
     * deletes the officially registered timer and adds it to pseudo registered
     *
     * @param officialTimer timer that is registered in Flink's timer service.
     * @param context       OnTimerContext in Flink's KeyedProcessFunction.
     * @param timersQueue   Timers that are pseudo registered (not officially registered in Apache Flink timer service)
     * @throws IOException
     */
    private void moveOfficialTimerToPseudoRegistered(TimerInKeyedProcess<E> officialTimer, KeyedProcessFunction.Context context,
                                                     SortedMap<Long, List<TimerInKeyedProcess<E>>> timersQueue) throws IOException {

        //unregister the officialTimer
        this.cleanStates(officialTimer, context);
        //put the previously officially registered timer in the the tree for timers waiting to be registered
        timersQueue.computeIfAbsent(officialTimer.getTime(), (key) -> {
            LinkedList<TimerInKeyedProcess<E>> queue = new LinkedList<>();
            return queue;
        });
        timersQueue.get(officialTimer.getTime()).add(officialTimer);
    }

    /**
     * returns current watermark if time charachteristics are set to event time
     * otherwise returns current processing time.
     *
     * @param context Context in Flink's KeyedProcessFunction.
     * @return
     */
    private long getCurrentTime(KeyedProcessFunction.Context context) {
        if (this.eventTime)
            return context.timerService().currentWatermark();
        return context.timerService().currentProcessingTime();
    }

    /**
     * checks if the triggeringTime is valid
     *
     * @param triggeringTime the triggering time that needs to be checked
     * @param context Context in Flink's KeyedProcessFunction.
     * @return
     */
    private boolean isValidTriggeringTime(long triggeringTime, KeyedProcessFunction.Context context) {
        if (triggeringTime < 0) {
            return Boolean.FALSE;
        }
        if (triggeringTime < this.getCurrentTime(context)) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

}