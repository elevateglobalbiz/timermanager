package biz.elevateglobal.timerManagerPackage.FullTimerManager.KeyedProcessFullTimerManager;

import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerKeyedProcessCallback;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;

/**
 * This class represents a timer which is used in KeyedProcessFunction.
 *
 * @param <E> - Type of the output elements.
 */
public class TimerInKeyedProcess<E> {

    /**
     * the time at which this timer should be fired.
     */
    private long time;

    /**
     * the callback which defines the behaviour which should be executed when this timer fires.
     */
    private IOnTimerKeyedProcessCallback<E> iOnTimer;

    /**
     * unique ID of the timer, this ID can be used for deleting the registered timer.
     */
    private int uniqueId;

    public TimerInKeyedProcess(long time, IOnTimerKeyedProcessCallback<E> iOnTimer) {
        this.time = time;
        this.iOnTimer = iOnTimer;
//        uniqueId = Objects.hash(time, iOnTimer, System.currentTimeMillis());
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public IOnTimerKeyedProcessCallback<E> getiOnTimer() {
        return iOnTimer;
    }

    public void setiOnTimer(IOnTimerKeyedProcessCallback<E> iOnTimer) {
        this.iOnTimer = iOnTimer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimerInKeyedProcess<?> timerInKeyedProcess = (TimerInKeyedProcess<?>) o;
        return time == timerInKeyedProcess.time &&
                uniqueId == timerInKeyedProcess.uniqueId;
    }

    public int getUID() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * execute the defined callback in this timer.
     *
     * @param context OnTimerContext in Flink's KeyedProcessFunction.
     * @param out     Collector in Flink's KeyedProcessFunction.
     * @throws IOException
     */
    public void fire(KeyedProcessFunction.OnTimerContext context, Collector<E> out) throws IOException {
        this.iOnTimer.onTimerFunction(this.time, context, out);

    }
}
