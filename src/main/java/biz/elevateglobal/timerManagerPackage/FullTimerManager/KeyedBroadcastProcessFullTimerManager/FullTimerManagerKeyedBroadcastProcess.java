package biz.elevateglobal.timerManagerPackage.FullTimerManager.KeyedBroadcastProcessFullTimerManager;

import biz.elevateglobal.timerManagerPackage.FullTimerManager.Utills.TimerFactory;
import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerKeyedBroadcast;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of Full Timer Manager which can be used in Keyed Broadcast Process Function.
 *
 * The Full Timer Manager have only one timer registered in Flink's timer service at any moment,
 * the others user-registered timers are fully managed by the timer manager.
 *
 * @param <E> Type of the output elements.
 */
public class FullTimerManagerKeyedBroadcastProcess<E> implements Serializable {


    private static final int FIRST = 0;
    /**
     * true if time characteristics are setted to EventTime, false if setted to ProcessingTime.
     */
    private Boolean eventTime;

    /**
     * this is the officially registered timer in Apache Flink.
     */
    private ValueState<TimerInKeyedBroadcastProcess<E>> registeredTimerState;

    /**
     * In this data structure are kept all other timers that ware added(pseudo registered) but will fire later
     * than the officially registered.
     */
    private ValueState<SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>>> timersQueueState;

    /**
     * key-timer map, where key is the unique Id of the timer, generated at It's creation.
     */
    private ValueState<Map<Integer, TimerInKeyedBroadcastProcess<E>>> timersUIDState;

    /**
     * this function must be called called from the Flink's onTimer function.
     *
     * @param ctx OnTimerContext in Flink's KeyedBroadcastProcessFunction.
     * @param out Collector in Flink's KeyedBroadcastProcessFunction.
     * @throws Exception
     */
    public void onTimer(KeyedBroadcastProcessFunction.OnTimerContext ctx, Collector<E> out) throws Exception {
        fireTimer(ctx, out);
        //check if any unofficially registered timer needs to be fired and remove it
        fireExpiredTimers(ctx, out);

        reorderTimers(ctx);
    }

    /**
     * Fires the officially registered timer in Flink's timer service
     *
     * @param ctx OnTimerContext in Flink's KeyedBroadcastProcessFunction.
     * @param out Collector in Flink's KeyedBroadcastProcessFunction.
     * @throws IOException
     */
    private void fireTimer(KeyedBroadcastProcessFunction.OnTimerContext ctx, Collector<E> out) throws IOException {
        registeredTimerState.value().fire(ctx, out);
        registeredTimerState.update(null);
    }


    /**
     * creates the Timer Manager and initialize the states.
     *
     * @param runtimeContext Flink's runtime context.
     * @param eventTime      boolean variable, true if time characteristics are set to event-time, otherwise is false.
     * @throws IOException
     */
    public FullTimerManagerKeyedBroadcastProcess(RuntimeContext runtimeContext, boolean eventTime) throws IOException {
        this.eventTime = eventTime;
        ValueStateDescriptor<TimerInKeyedBroadcastProcess<E>> registeredTimerDesc = new ValueStateDescriptor<TimerInKeyedBroadcastProcess<E>>(
                "reg-timer",
                TypeInformation.of(new TypeHint<TimerInKeyedBroadcastProcess<E>>() {
                })
        );
        this.registeredTimerState = runtimeContext.getState(registeredTimerDesc);

        ValueStateDescriptor<SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>>> timersForRegDesc =
                new ValueStateDescriptor<SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>>>(
                        "timers-for-reg",
                        TypeInformation.of(
                                new TypeHint<SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>>>() {
                                })
                );
        this.timersQueueState = runtimeContext.getState(timersForRegDesc);

        ValueStateDescriptor<Map<Integer, TimerInKeyedBroadcastProcess<E>>> addedTimersDescription =
                new ValueStateDescriptor<Map<Integer, TimerInKeyedBroadcastProcess<E>>>(
                        "added-timers",
                        TypeInformation.of(
                                new TypeHint<Map<Integer, TimerInKeyedBroadcastProcess<E>>>() {
                                })
                );
        this.timersUIDState = runtimeContext.getState(addedTimersDescription);

    }

    /**
     * initialize the states in the timer manager
     *
     * @throws IOException
     */
    private void initializeStates() throws IOException {
        //in case of recovering after failure or the states are already initialized do nothing, otherwise initialize the states
        if (timersUIDState.value() != null)
            return;
        timersQueueState.update(new TreeMap<>(Comparator.comparing(Long::longValue)));
        timersUIDState.update(new HashMap<>());

    }

    /**
     * register a timer at the specified triggering time with callback onTimerFunct (which will be executed when the timer fires).
     *
     * @param triggeringTime  time at which the timer will be registered (if the time is not in the past).
     * @param onTimerCallback Implemented IOnTimerKeyedBroadcast (callback function).
     * @param readOnlyContext ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @return unique ID of the registered timer, only with this ID this timer can be deleted later. If the triggering time of the timer
     * is not valid -1 is returned
     * @throws IOException
     */
    public int registerTimer(long triggeringTime, IOnTimerKeyedBroadcast onTimerCallback, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) throws IOException {

        if (!this.isValidTriggeringTime(triggeringTime, readOnlyContext))
            return -1;

        TimerInKeyedBroadcastProcess<E> newTimer = TimerFactory.<E>createTimer(triggeringTime, onTimerCallback);

        initializeStates();

        addTimer(newTimer, readOnlyContext);

        reorderTimers(readOnlyContext);

        trackTimer(newTimer);

        return newTimer.getUID();
    }

    /**
     * Adds the new timer (newTimer) to the pseudo registered and then does reordering of the timers to make sure
     * that the the timer that will fire first is officially registered in Flink's timer service
     *
     * @param newTimer        the timer that will be registered
     * @param readOnlyContext ReadOnlyContext in Flink's KeyedBroadcastProcessFunction
     * @throws IOException
     */
    private void addTimer(TimerInKeyedBroadcastProcess<E> newTimer, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) throws IOException {
        SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>> timersForReg = timersQueueState.value();

        timersForReg.computeIfPresent(newTimer.getTime(), (timer, queue) -> {
            queue.add(newTimer);
            return queue;
        });

        timersForReg.computeIfAbsent(newTimer.getTime(), (timer) -> {
            ArrayList<TimerInKeyedBroadcastProcess<E>> l = new ArrayList<>();
            l.add(newTimer);
            return l;
        });

        timersQueueState.update(timersForReg);
    }


    /**
     * unregister the timer from Apache Flink's timer service.
     *
     * @param timerToBeUnregistered the timer that needs to be unregistered.
     * @param readOnlyContext       ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     */
    private void unregisterTimer(TimerInKeyedBroadcastProcess<E> timerToBeUnregistered, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) {
        if (this.eventTime) {
            readOnlyContext.timerService().deleteEventTimeTimer(timerToBeUnregistered.getTime());
            return;
        }
        readOnlyContext.timerService().deleteProcessingTimeTimer(timerToBeUnregistered.getTime());
    }


    /**
     * deletes the timer passed as argument from the pseudo and officially registered, but do not remove it's track (It's Id).
     *
     * @param timerToBeDeleted timer that will to be deleted.
     * @param readOnlyContext  ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @throws IOException
     */
    private void cleanStates(TimerInKeyedBroadcastProcess<E> timerToBeDeleted, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) throws IOException {
        boolean deleted = deleteOfficialTimer(timerToBeDeleted, readOnlyContext);
        if (deleted)
            return;

        SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>> timersForReg = timersQueueState.value();
        this.removeTimerFromDataStructure(timerToBeDeleted, timersForReg);
    }

    /**
     * checks if the timer passed as an argument is officially registered in Apache Flink's timer service.
     * If yes then unregister it, otherwise does nothing
     *
     * @param timerToBeDeleted the timer that needs to be unregistered if it is officially registered in Apache Flink's timer service
     * @param readOnlyContext  ReadOnlyContext in Flink's KeyedBroadcastProcessFunction
     * @return
     * @throws IOException
     */
    private boolean deleteOfficialTimer(TimerInKeyedBroadcastProcess<E> timerToBeDeleted, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) throws IOException {
        TimerInKeyedBroadcastProcess<E> registeredTimer = registeredTimerState.value();
        if (registeredTimer != null && registeredTimer.equals(timerToBeDeleted)) {
            registeredTimerState.update(null);
            unregisterTimer(timerToBeDeleted, readOnlyContext);
            return true;
        }
        return false;
    }

    /**
     * removes the timer timerToBeRemoved from the data structure for pseudo registered timers
     *
     * @param timerToBeRemoved the timer that will be removed from data structure for pseudo registered timers.
     * @param timersForReg     data structure where the pseudo registered timers are stored.
     * @throws IOException
     */
    private void removeTimerFromDataStructure(TimerInKeyedBroadcastProcess<E> timerToBeRemoved, SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>> timersForReg) throws IOException {
        List<TimerInKeyedBroadcastProcess<E>> queue = timersForReg.getOrDefault(timerToBeRemoved.getTime(), null);
        if (queue == null)
            return;
        queue.remove(timerToBeRemoved);

        if (queue.size() == 0)
            timersForReg.remove(timerToBeRemoved.getTime());
        timersQueueState.update(timersForReg);
    }

    /**
     * deletes everything about the timer specified with timerToDeleteID (It's ID), if it exists.
     *
     * @param timerToDeleteID the ID of the timer that will be deleted.
     * @param readOnlyContext ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @throws IOException
     */
    public void unregisterTimer(int timerToDeleteID, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) throws IOException {
        if (!isTimerRegistered(timerToDeleteID)) {
            return;
        }

        Map<Integer, TimerInKeyedBroadcastProcess<E>> timersUID = this.timersUIDState.value();
        TimerInKeyedBroadcastProcess<E> timerToBeDeleted = timersUID.get(timerToDeleteID);

        untrackTimer(timerToBeDeleted);

        cleanStates(timerToBeDeleted, readOnlyContext);

        reorderTimers(readOnlyContext);
    }

    /**
     * checks if the there is registered timer with the specified ID.
     *
     * @param timerToDeleteID the Id of the timer.
     * @return true if the timer with the specified Id exists otherwise returns false.
     * @throws IOException
     */
    private boolean isTimerRegistered(int timerToDeleteID) throws IOException {
        Map<Integer, TimerInKeyedBroadcastProcess<E>> timersUID = this.timersUIDState.value();
        return timersUID.containsKey(timerToDeleteID);
    }

    /**
     * removes the timer passed as an argument from the tracked timers(timersUIDState).
     *
     * @param timerToBeDeleted the timer which will be removed from timersUIDState.
     * @throws IOException
     */
    private void untrackTimer(TimerInKeyedBroadcastProcess<E> timerToBeDeleted) throws IOException {
        Map<Integer, TimerInKeyedBroadcastProcess<E>> timersUID = this.timersUIDState.value();

        timersUID.remove(timerToBeDeleted.getUID());

        this.timersUIDState.update(timersUID);
    }

    /**
     * maps the ID and the corresponding timer in the map addedTimers.
     *
     * @param timerToTrack - the timer that will be tracked with it's ID.
     * @throws IOException
     */
    private void trackTimer(TimerInKeyedBroadcastProcess<E> timerToTrack) throws IOException {
        Map<Integer, TimerInKeyedBroadcastProcess<E>> timersUID = this.timersUIDState.value();
        timersUID.put(timerToTrack.getUID(), timerToTrack);
        this.timersUIDState.update(timersUID);
    }

    /**
     * register the timer passed as an argument using Apache Flink's timer service.
     *
     * @param timerToRegisterInFlink the timer that will be officially registered.
     * @param readOnlyContext        ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @throws IOException
     */
    private void registerTimer(TimerInKeyedBroadcastProcess<E> timerToRegisterInFlink, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) throws IOException {
        if (this.eventTime) {
            readOnlyContext.timerService().registerEventTimeTimer(timerToRegisterInFlink.getTime());
        } else {
            readOnlyContext.timerService().registerProcessingTimeTimer(timerToRegisterInFlink.getTime());
        }
        registeredTimerState.update(timerToRegisterInFlink);
    }


    /**
     * checks if any unofficially registered (not registered in Flink's timer service) timer needs to be fired
     * if yes, then call artificialOnTimer with the timer as argument.
     *
     * @param ctx OnTimerContext in Flink's KeyedBroadcastProcessFunction.
     * @param out Collector in Flink's KeyedBroadcastProcessFunction.
     * @throws IOException
     */
    private void fireExpiredTimers(KeyedBroadcastProcessFunction.OnTimerContext ctx, Collector<E> out) throws IOException {

        long currentTime = this.getCurrentTime(ctx);
        SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>> timersQueue = this.timersQueueState.value();

        if (timersQueue.size() == 0)
            return;

        timersQueue.keySet().stream().filter(triggeringTime -> triggeringTime < currentTime).map(timersQueue::get)
                .forEach(timersQueueForFiring -> this.fireAllTimers(timersQueueForFiring, ctx, out));

        List<Long> expiredTriggeringTimes = timersQueue.keySet().stream().filter(triggerTime -> triggerTime < currentTime)
                .collect(Collectors.toList());

        expiredTriggeringTimes.stream().forEach(tirggeringTime -> timersQueue.remove(tirggeringTime));

        this.timersQueueState.update(timersQueue);
    }

    /**
     * fires all the timers that are in the list timersQueueForFiring
     *
     * @param timersQueueForFiring a list containing timers that will are triggered and will be fired
     * @param ctx                  OnTimerContext in Flink's KeyedBroadcastProcessFunction.
     * @param out                  Collector in Flink's KeyedBroadcastProcessFunction..
     */
    private void fireAllTimers(List<TimerInKeyedBroadcastProcess<E>> timersQueueForFiring, KeyedBroadcastProcessFunction.OnTimerContext ctx,
                               Collector<E> out) {
        timersQueueForFiring.forEach(timerForFiring -> {
            try {
                timerForFiring.fire(ctx, out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * The main logic of the timer management is done here.
     * Checks if there is unofficially registered timer that should ne fired earlier than the officially registered
     * if yes then swaps them, otherwise do nothing.
     *
     * @param readOnlyContext ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @throws IOException
     */
    private void reorderTimers(KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) throws IOException {
        //timers that are psuedo registered (registered in the timer menager)
        SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>> timersQueue = this.timersQueueState.value();
        if (timersQueue.size() == 0)
            return;
        //timer that is officially registered in Flink's timer service
        TimerInKeyedBroadcastProcess<E> officiallyRegisteredTimer = registeredTimerState.value();

        //the timer that will fire first from the unofficially registered timers
        TimerInKeyedBroadcastProcess<E> nextTimerCandidate = timersQueue.get(timersQueue.firstKey()).get(FIRST);

        if (officiallyRegisteredTimer != null && officiallyRegisteredTimer.getTime() < nextTimerCandidate.getTime())
            //there is already registered timer and the candidate for nextTimer will happen after the officially registered
            return;
        else if (officiallyRegisteredTimer != null && officiallyRegisteredTimer.getTime() > nextTimerCandidate.getTime()) {
            //the candidate for nextTimer needs to be fired before the officially registered
            //unregister the officially registered and add to the queued
            this.moveOfficialTimerToPseudoRegistered(officiallyRegisteredTimer, readOnlyContext, timersQueue);
            officiallyRegisteredTimer = null;
        }
        //at the end do the check if there is an officially registered timer at all
        //if not register the next candidate(the one with smallest time for firing)
        if (officiallyRegisteredTimer == null) {
            //this will delete timer from where it currently exists - the pseudo registered timers(timersForRegistering)
            this.cleanStates(nextTimerCandidate, readOnlyContext);
            //register the timer that should fire first
            this.registerTimer(nextTimerCandidate, readOnlyContext);
        }
        this.timersQueueState.update(timersQueue);
    }


    /**
     * deletes the officially registered timer and adds it to the pseudo registered
     *
     * @param officialTimer   timer that is registered in Flink's timer service.
     * @param readOnlyContext ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @param timersQueue     Timers that are pseudo registered (not officially registered in Apache Flink timer service)
     * @throws IOException
     */
    private void moveOfficialTimerToPseudoRegistered(TimerInKeyedBroadcastProcess<E> officialTimer, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext,
                                                     SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>> timersQueue) throws IOException {

        //unregister the officialTimer
        this.cleanStates(officialTimer, readOnlyContext);
        //put the previously officially registered timer in the the tree for timers waiting to be registered
        timersQueue.computeIfAbsent(officialTimer.getTime(), (key) -> {
            LinkedList<TimerInKeyedBroadcastProcess<E>> queue = new LinkedList<>();
            return queue;
        });
        timersQueue.get(officialTimer.getTime()).add(officialTimer);
    }

    /**
     * returns current watermark if time charachteristics are set to event time
     * otherwise returns current processing time.
     *
     * @param readOnlyContext ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @return
     */
    private long getCurrentTime(KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) {
        if (this.eventTime)
            return readOnlyContext.timerService().currentWatermark();
        return readOnlyContext.timerService().currentProcessingTime();
    }

    /**
     * checks if the triggeringTime is valid
     *
     * @param triggeringTime the triggering time that needs to be checked
     * @param readOnlyContext ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @return
     */
    private boolean isValidTriggeringTime(long triggeringTime, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) {
        if (triggeringTime < 0) {
            return Boolean.FALSE;
        }
        if (triggeringTime < this.getCurrentTime(readOnlyContext)) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

}
