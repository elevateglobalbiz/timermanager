package biz.elevateglobal.timerManagerPackage.TimerManager.Utills;

import java.util.Objects;

public class GenerateTimerUID {

    public static int generateTimerUID(long triggeringTime) {
        return Objects.hash(triggeringTime, System.currentTimeMillis(), Math.random() * 100000);
    }
}
