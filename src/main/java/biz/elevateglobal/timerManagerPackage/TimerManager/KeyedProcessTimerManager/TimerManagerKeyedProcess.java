package biz.elevateglobal.timerManagerPackage.TimerManager.KeyedProcessTimerManager;

import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerKeyedProcessCallback;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * Implementation of Timer Manager which can be used in keyed Process function.
 *
 * @param <E> Type of the output elements.
 */
public class TimerManagerKeyedProcess<E> implements Serializable {

//    private static final int FIRST = 0;
    /**
     * true if time characteristics are setted to EventTime, false if setted to ProcessingTime.
     */
    private Boolean eventTime;

    /**
     * In this data structure are kept all timers that ware added. The ones that are officially registered and
     * the ones pseudo registered
     */
    private ValueState<SortedMap<Long, List<TimerInKeyedProcess<E>>>> registeredTimersSortedMap;

    /**
     * key-timer map. where key is the unique Id of the timer, generated at It's creation.
     */
    private ValueState<Map<Integer, TimerInKeyedProcess<E>>> timersUIDHashMap;




    /**
     * creates the Timer Manager and initialize the states.
     *
     * @param runtimeContext Flink's runtime context.
     * @param eventTime      boolean variable, true if time characteristics are set to event-time, otherwise is false.
     * @throws IOException
     */
    public TimerManagerKeyedProcess(RuntimeContext runtimeContext, boolean eventTime) throws IOException {
        ValueStateDescriptor<SortedMap<Long, List<TimerInKeyedProcess<E>>>> timersForRegDesc =
                new ValueStateDescriptor<SortedMap<Long, List<TimerInKeyedProcess<E>>>>(
                        "timers-for-reg",
                        TypeInformation.of(
                                new TypeHint<SortedMap<Long, List<TimerInKeyedProcess<E>>>>() {
                                })
                );
        this.registeredTimersSortedMap = runtimeContext.getState(timersForRegDesc);

        ValueStateDescriptor<Map<Integer, TimerInKeyedProcess<E>>> addedTimersDescription =
                new ValueStateDescriptor<Map<Integer, TimerInKeyedProcess<E>>>(
                        "added-timers",
                        TypeInformation.of(
                                new TypeHint<Map<Integer, TimerInKeyedProcess<E>>>() {
                                })
                );
        this.timersUIDHashMap = runtimeContext.getState(addedTimersDescription);

        this.eventTime = eventTime;
    }

    /**
     * initialize the states in the timer manager
     *
     * @throws IOException
     */
//    private void initializeStates() throws IOException {
//        //in case of recovering after failure or in case the states are already initialized do nothing
//        if (timersUIDState.value() == null) {
//            this.registeredTimersState.update(new TreeMap<>(Comparator.comparing(Long::longValue)));
//            this.timersUIDState.update(new HashMap<>());
//        }
//    }

    /**
     * register a timer at time time with callback onTimerFunct (which will be executed when the timer fires).
     *
     * @param triggeringTime time at which the timer will be registered (if the time is not in the past).
     * @param onTimerFunct   Implemented IOnTimerKeyedProcess (callback function).
     * @param context        Context in Flink's KeyedProcessFunction.
     * @return unique ID of the registered timer, only with this ID this timer can be deleted later. If the triggering time of the timer
     * is not valid -1 is returned
     * @throws IOException
     */
    public int registerTimer(long triggeringTime, IOnTimerKeyedProcessCallback onTimerCallback, KeyedProcessFunction.Context context) throws IOException {

//        if (!this.isValidTriggeringTime(triggeringTime, context))
//            return -1;
//
//        initializeStates();
//
//        TimerInKeyedProcess<E> newTimerInKeyedProcess = TimerFactory.<E>createTimer(triggeringTime, onTimerFunct);
//
//        trackTimer(newTimerInKeyedProcess);
//
//        addTimer(newTimerInKeyedProcess, context);
//
//        return newTimerInKeyedProcess.getUID();
        return 1;
    }


    /**
     * unregister the timer from Apache Flink's timer service.
     *
     * @param timerToBeUnregistered the timer that needs to be unregistered.
     * @param context               Context in Flink's KeyedProcessFunction.
     */
//    private void unregisterTimer(TimerInKeyedProcess<E> timerToBeUnregistered, KeyedProcessFunction.Context context) {
//        if (this.eventTime) {
//            context.timerService().deleteEventTimeTimer(timerToBeUnregistered.getTime());
//            return;
//        }
//        context.timerService().deleteProcessingTimeTimer(timerToBeUnregistered.getTime());
//    }


    /**
     * deletes the timer passed as argument from the registered timers, but do not remove it's track (It's Id).
     *
     * @param timerToBeDeleted timer that will to be deleted.
     * @throws IOException
     */
//    private void cleanState(TimerInKeyedProcess<E> timerToBeDeleted, KeyedProcessFunction.Context context) throws IOException {
//        SortedMap<Long, List<TimerInKeyedProcess<E>>> registeredTimers = this.registeredTimersState.value();
//
//        removeTimerFromDataStructure(timerToBeDeleted, registeredTimers);
//    }

    /**
     * removes the timerToBeRemoved from the data structure for registered timers
     *
     * @param timerToBeRemoved the timer that will be removed from the registered timers
     * @param registeredTimers the data stracture that contains all the registered timers
     * @throws IOException
     */
//    private void removeTimerFromDataStructure(TimerInKeyedProcess<E> timerToBeRemoved, SortedMap<Long,
//            List<TimerInKeyedProcess<E>>> registeredTimers) throws IOException {
//
//        List<TimerInKeyedProcess<E>> queue = registeredTimers.getOrDefault(timerToBeRemoved.getTime(), null);
//
//        if (queue == null)
//            return;
//        queue.remove(timerToBeRemoved);
//
//        if (queue.size() == 0)
//            registeredTimers.remove(timerToBeRemoved.getTime());
//
//        this.registeredTimersState.update(registeredTimers);
//    }


    /**
     * deletes everything about the timer specified with timerToDeleteID (It's ID), if it exists.
     *
     * @param timerToDeleteID the ID of the timer that you want delete.
     * @param context         Context in Flink's KeyedProcessFunction.
     * @throws IOException
     */
    public void unregisterTimer(int timerToDeleteID, KeyedProcessFunction.Context context) throws IOException {

//        if (!isTimerRegistered(timerToDeleteID)) {
//            return;
//        }
//
//        Map<Integer, TimerInKeyedProcess<E>> timersUID = this.timersUIDState.value();
//        TimerInKeyedProcess<E> timerToBeDeleted = timersUID.get(timerToDeleteID);
//
//        untrackTimer(timerToBeDeleted);
//
//        cleanState(timerToBeDeleted, context);
//
//        keepConsistentState(context, registeredTimersState.value());
    }

    /**
     * removes the timer passed as an argument from the tracked timers(timersUIDState).
     *
     * @param timerToBeDeleted the timer which will be removed from timersUIDState.
     * @throws IOException
     */
//    private void untrackTimer(TimerInKeyedProcess<E> timerToBeDeleted) throws IOException {
//        Map<Integer, TimerInKeyedProcess<E>> timersUID = this.timersUIDState.value();
//
//        timersUID.remove(timerToBeDeleted.getUID());
//
//        this.timersUIDState.update(timersUID);
//    }

    /**
     * checks if the there is registered timer with the specified ID.
     *
     * @param timerToDeleteID the Id of the timer.
     * @return true if the timer with the specified Id exists otherwise returns false.
     * @throws IOException
     */
//    private boolean isTimerRegistered(int timerToDeleteID) throws IOException {
//        Map<Integer, TimerInKeyedProcess<E>> timersUID = this.timersUIDState.value();
//        return timersUID.containsKey(timerToDeleteID);
//    }


    /**
     * maps the ID and the corresponding timer in the map addedTimers.
     *
     * @param timerToTrack the timer that will be tracked with it's id.
     * @throws IOException
     */
//    private void trackTimer(TimerInKeyedProcess<E> timerToTrack) throws IOException {
//        Map<Integer, TimerInKeyedProcess<E>> addedTimersMap = timersUIDState.value();
//        addedTimersMap.put(timerToTrack.getUID(), timerToTrack);
//        this.timersUIDState.update(addedTimersMap);
//    }

    /**
     * register the timer passed as an argument using Apache Flink's timer service.
     *
     * @param timerToRegisterInFlink the timer that will be officially registered.
     * @param context                Context in Flink's KeyedProcessFunction.
     * @throws IOException
     */
//    private void registerTimer(TimerInKeyedProcess<E> timerToRegisterInFlink, KeyedProcessFunction.Context context) throws IOException {
//        if (this.eventTime) {
//            context.timerService().registerEventTimeTimer(timerToRegisterInFlink.getTime());
//        } else {
//            context.timerService().registerProcessingTimeTimer(timerToRegisterInFlink.getTime());
//        }
//    }


    /**
     * checks if any unofficially registered (not registered in Flink's timer service) timer needs to be fired
     * if yes, then call call artificialOnTimer with the timer as argument.
     *
     * @param context OnTimerContext in Flink's KeyedProcessFunction.
     * @param out     Collector in Flink's KeyedProcessFunction.
     * @throws IOException
     */
//    private void fireExpiredTimers(KeyedProcessFunction.OnTimerContext context, Collector<E> out) throws IOException {
//
//        long currentTime = this.getCurrentTime(context);
//        SortedMap<Long, List<TimerInKeyedProcess<E>>> timersQueue = this.registeredTimersState.value();
//
//        if (timersQueue.size() == 0)
//            return;
//
//        timersQueue.keySet().stream().filter(triggeringTime -> triggeringTime < currentTime).map(timersQueue::get)
//                .forEach(timersQueueForFiring -> this.fireAllTimers(timersQueueForFiring, context, out));
//
//        List<Long> expiredTriggeringTimes = timersQueue.keySet().stream().filter(triggerTime -> triggerTime < currentTime)
//                .collect(Collectors.toList());
//
//        expiredTriggeringTimes.stream().forEach(tirggeringTime -> timersQueue.remove(tirggeringTime));
//
//        this.registeredTimersState.update(timersQueue);
//    }

    /**
     * fires all the timers that are in the passed list timersQueueForFiring
     *
     * @param timersQueueForFiring a list containing timers that will are triggered and will be fired
     * @param context              OnTimerContext in Flink's KeyedProcessFunction.
     * @param out                  Collector in Flink's KeyedProcessFunction.
     */
//    private void fireAllTimers(List<TimerInKeyedProcess<E>> timersQueueForFiring, KeyedProcessFunction.OnTimerContext context,
//                               Collector<E> out) {
//        timersQueueForFiring.forEach(timerForFiring -> {
//            try {
//                timerForFiring.fire(context, out);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        });
//    }

    /**
     * The main logic of the timer management is done here.
     * Checks if there is already registered timer in Flink's timer service at the same timestamp as the new timer triggering timestamp
     * if not then registers the new timer in Flink's timer service and adds it to the list, otherwise just adds it to the list of timers with
     * same triggering timestamp
     *
     * @param context Context in Flink's KeyedProcessFunction.
     * @throws IOException
     */
//    private void addTimer(TimerInKeyedProcess<E> newTimer, KeyedProcessFunction.Context context) throws IOException {
//        //timers that are psudo registered (registered in the timer menager)
//        SortedMap<Long, List<TimerInKeyedProcess<E>>> registeredTimers = this.registeredTimersState.value();
//
//        //if timer with this timestamp has not been registered in Flink timer service so far
//        if (!registeredTimers.containsKey(newTimer.getTime())) {
//            //timer that is officially registered in Flink's timer service
//            this.registerTimer(newTimer, context);
//            newTimer.setOfficial(Boolean.TRUE);
//        }
//
//        registeredTimers.computeIfPresent(newTimer.getTime(), (timer, queue) -> {
//            queue.add(newTimer);
//            return queue;
//        });
//        registeredTimers.computeIfAbsent(newTimer.getTime(), (timer) -> {
//            ArrayList<TimerInKeyedProcess<E>> l = new ArrayList<>();
//            l.add(newTimer);
//            return l;
//        });
//
//        this.registeredTimersState.update(registeredTimers);
//
//    }

    /**
     * makes sure that for all different triggering times in the map there is one officially registered timer
     *
     * @param context          Context in Flink's KeyedProcessFunction.
     * @param registeredTimers SortedMap in which all the timers are stored (triggering time-> list of timers that should
     *                         fire at that timestamp), and sorted by their triggering time
     */
//    private void keepConsistentState(KeyedProcessFunction.Context context, SortedMap<Long, List<TimerInKeyedProcess<E>>> registeredTimers) {
//        registeredTimers.entrySet().stream()
//                .filter(entry -> {
//                    return entry.getValue().stream().noneMatch(TimerInKeyedProcess::isOfficial);
//                })
//                .forEach(entry -> {
//                    try {
//                        this.registerTimer(entry.getValue().get(FIRST), context);
//                        entry.getValue().get(FIRST).setOfficial(Boolean.TRUE);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                });
//    }

    /**
     * returns current watermark if time charachteristics are set to event time
     * otherwise returns current processing time.
     *
     * @param context Context in Flink's KeyedProcessFunction.
     * @return
     */
//    private long getCurrentTime(KeyedProcessFunction.Context context) {
//        if (this.eventTime)
//            return context.timerService().currentWatermark();
//        return context.timerService().currentProcessingTime();
//    }

    /**
     * checks if the triggeringTime is valid
     *
     * @param triggeringTime the triggering time that needs to be checked
     * @param context Context in Flink's KeyedProcessFunction.
     * @return
     */
//    private boolean isValidTriggeringTime(long triggeringTime, KeyedProcessFunction.Context context) {
//        if (triggeringTime < 0) {
//            return Boolean.FALSE;
//        }
//        if (triggeringTime < this.getCurrentTime(context)) {
//            return Boolean.FALSE;
//        }
//        return Boolean.TRUE;
//    }

    /**
     * this function is called from the Flink's onTimer function.
     *
     * @param ctx OnTimerContext in Flink's KeyedProcessFunction.
     * @param out Collector in Flink's KeyedProcessFunction.
     * @throws Exception
     */
    public void onTimer(KeyedProcessFunction.OnTimerContext context, Collector<E> out) throws Exception {
        //check if any unofficially registered timer needs to be fired and remove it
//        fireExpiredTimers(ctx, out);
    }
}

