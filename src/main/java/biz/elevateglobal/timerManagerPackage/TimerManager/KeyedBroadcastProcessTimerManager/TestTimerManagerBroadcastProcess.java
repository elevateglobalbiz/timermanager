package biz.elevateglobal.timerManagerPackage.TimerManager.KeyedBroadcastProcessTimerManager;

import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerKeyedBroadcast;
import biz.elevateglobal.messages.Instance;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Example of using and utilizing Timer Manager(TimerManagerKeyedBroadcastProcess) in KeyedBroadcastProcessFunction
 */
public class TestTimerManagerBroadcastProcess extends KeyedBroadcastProcessFunction<String, Instance, String, String> {

    /**
     * boolean variable about the setted time characteristics.
     */
    boolean eventTime;

    /**
     * broadcast state descriptor
     */
    MapStateDescriptor<Void, String> mapStateDescriptor;

    /**
     * a reference to the Timer Manager
     */
    TimerManagerKeyedBroadcastProcess<String> timerManager;

    /**
     * keep evidence of the registered timers and their IDs
     */
    ValueState<Map<Long, Integer>> regTimersUID;


    public TestTimerManagerBroadcastProcess(boolean eventTime, MapStateDescriptor<Void, String> mapStateDescriptor) throws IOException {
        this.eventTime = eventTime;
        this.mapStateDescriptor = mapStateDescriptor;
    }

    private void initializeStates() throws IOException {
        //in case of recovering after failure or the states are already initialized do nothing, otherwise initialize the states
        if (regTimersUID.value() != null)
            return;
        regTimersUID.update(new HashMap<>());
    }

    // In order to instantiate the states in the timer manager, runtumeContext is passed in it’s constructor.
    @Override
    public void open(Configuration parameters) throws Exception {
        // and boolean value about the time charachteristic to its constructor
        timerManager = new TimerManagerKeyedBroadcastProcess<>(getRuntimeContext(), eventTime);

        ValueStateDescriptor<Map<Long, Integer>> registeredTimersUIDDescriptor = new ValueStateDescriptor<Map<Long, Integer>>(
                "reg-timers-uid",
                TypeInformation.of(new TypeHint<Map<Long, Integer>>() {
                })
        );

        regTimersUID = getRuntimeContext().getState(registeredTimersUIDDescriptor);
    }

    // This is the official Apache Flink's onTimer function.
    // The onTimer function of the Timer Manager must be call here for proper functioning of the Timer Manager.
    @Override
    public void onTimer(long timestamp, OnTimerContext ctx, Collector<String> out) throws Exception {
        timerManager.onTimer(ctx, out);
    }

    //The registering and deleting od the timers is done here
    //when registering, the addTimer(...) from TimerManager returns unique Id of the registered timer
    //this id needs to be saved if you like to delete the corresponding timer later
    @Override
    public void processElement(Instance value, ReadOnlyContext ctx, Collector<String> out) throws Exception {
        this.initializeStates();
        long triggeringTime;

        long currentWatermark = ctx.timerService().currentWatermark();

        if (value.getTimestamp() % 2 == 0) {
            triggeringTime = currentWatermark + 3;
        } else {
            triggeringTime = currentWatermark + 10;
        }

        IOnTimerKeyedBroadcast<String> iOnTimerKeyedBroadcast = (timestamp, context, collector) ->
                collector.collect("Timer registered at " + (timestamp) + " from instance with timestamp "
                        + value.getTimestamp() + " has fired");

        int newTimerUniqueId = timerManager.registerTimer(triggeringTime, iOnTimerKeyedBroadcast, ctx);
        if (newTimerUniqueId == -1)
            return;

        System.out.println("Instance with timestamp " + value.getTimestamp() + " and key=" + ctx.getCurrentKey() + "\n"
                + "Current watermark " + currentWatermark + "\n"
                + "Registered timer on " + (triggeringTime));

        Map<Long, Integer> regTimersUID = this.regTimersUID.value();

        regTimersUID.put(value.getTimestamp(), newTimerUniqueId);

        this.regTimersUID.update(regTimersUID);
    }

    //process the control message
    @Override
    public void processBroadcastElement(String value, Context ctx, Collector<String> out) throws Exception {
        //for simplicity the new control message is just putted in the broadcast state
        ctx.getBroadcastState(this.mapStateDescriptor).put(null, value);
    }
}
