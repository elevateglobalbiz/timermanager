package biz.elevateglobal.timerManagerPackage.TimerManager.KeyedBroadcastProcessTimerManager;


import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerKeyedBroadcast;
import biz.elevateglobal.timerManagerPackage.TimerManager.Utills.TimerFactory;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of Timer Manager which can be used in KeyedBroadcastProcessFunction.
 *
 * @param <E> Type of the output elements.
 */
public class TimerManagerKeyedBroadcastProcess<E> implements Serializable {

    private static final int FIRST = 0;
    /**
     * true if time characteristics are setted to EventTime, false if setted to ProcessingTime.
     */
    private Boolean eventTime;

    /**
     * In this data structure are kept all timers that ware added. The ones that are officially registered and
     * the ones pseudo registered
     */
    private ValueState<SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>>> registeredTimersState;

    /**
     * key-timer map. where key is the unique Id of the timer, generated at It's creation.
     */
    private ValueState<Map<Integer, TimerInKeyedBroadcastProcess<E>>> timersUIDState;

    /**
     * this function is called from the Flink's onTimer function.
     *
     * @param ctx OnTimerContext in Flink's KeyedBroacastProcessFunction.
     * @param out Collector in Flink's KeyedBroacastProcessFunction.
     * @throws Exception
     */
    public void onTimer(KeyedBroadcastProcessFunction.OnTimerContext ctx, Collector<E> out) throws Exception {
        //check if any unofficially registered timer needs to be fired and remove it
        fireExpiredTimers(ctx, out);
    }


    /**
     * creates the Timer Manager and initialize the states.
     *
     * @param runtimeContext-Flink's Keyed Process Function runtime context.
     * @param eventTime              boolean variable, true if time characteristics are set to event-time, otherwise is false.
     * @throws IOException
     */
    public TimerManagerKeyedBroadcastProcess(RuntimeContext runtimeContext, boolean eventTime) throws IOException {
        ValueStateDescriptor<SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>>> timersForRegDesc =
                new ValueStateDescriptor<SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>>>(
                        "timers-for-reg",
                        TypeInformation.of(
                                new TypeHint<SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>>>() {
                                })
                );
        this.registeredTimersState = runtimeContext.getState(timersForRegDesc);

        ValueStateDescriptor<Map<Integer, TimerInKeyedBroadcastProcess<E>>> addedTimersDescription =
                new ValueStateDescriptor<Map<Integer, TimerInKeyedBroadcastProcess<E>>>(
                        "added-timers",
                        TypeInformation.of(
                                new TypeHint<Map<Integer, TimerInKeyedBroadcastProcess<E>>>() {
                                })
                );
        this.timersUIDState = runtimeContext.getState(addedTimersDescription);

        this.eventTime = eventTime;
    }

    /**
     * initialize the states in the timer manager
     *
     * @throws IOException
     */
    private void initializeStates() throws IOException {
        //in case of recovering after failure or in case the states are already initialized do nothing
        if (timersUIDState.value() == null) {
            this.registeredTimersState.update(new TreeMap<>(Comparator.comparing(Long::longValue)));
            this.timersUIDState.update(new HashMap<>());
        }
    }

    /**
     * register a timer at time time with callback onTimerFunct (which will be executed when the timer fires).
     *
     * @param triggeringTime-time at which the timer will be registered (if the time is not in the past).
     * @param onTimerFunct-       Implemented IOnTimerKeyedProcess (callback function).
     * @param readOnlyContext     ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @return unique ID of the registered timer, only with this ID this timer can be deleted later. If the triggering time of the timer
     * is not valid -1 is returned
     * @throws IOException
     */
    public int registerTimer(long triggeringTime, IOnTimerKeyedBroadcast onTimerFunct, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) throws IOException {

        this.initializeStates();

        if (!this.isValidTriggeringTime(triggeringTime, readOnlyContext))
            return -1;

        TimerInKeyedBroadcastProcess<E> newTimer = TimerFactory.<E>createTimer(triggeringTime, onTimerFunct);

        this.trackTimer(newTimer);

        this.addTimer(newTimer, readOnlyContext);

        return newTimer.getUID();
    }


    /**
     * unregister the timer from Apache Flink's timer service.
     *
     * @param timerToBeUnregistered the timer that needs to be unregistered.
     * @param readOnlyContext       ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     */
    private void unregisterTimer(TimerInKeyedBroadcastProcess<E> timerToBeUnregistered, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) {
        if (this.eventTime) {
            readOnlyContext.timerService().deleteEventTimeTimer(timerToBeUnregistered.getTime());
            return;
        }
        readOnlyContext.timerService().deleteProcessingTimeTimer(timerToBeUnregistered.getTime());
    }


    /**
     * deletes the timer passed as argument from the registered timers, but do not remove it's track (It's Id).
     *
     * @param timerToBeDeleted timer that will to be deleted.
     * @throws IOException
     */
    private void cleanState(TimerInKeyedBroadcastProcess<E> timerToBeDeleted) throws IOException {
        SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>> registeredTimers = this.registeredTimersState.value();

        removeTimerFromDataStructure(timerToBeDeleted, registeredTimers);
    }

    /**
     * removes the timerToBeRemoved from the data structure for registered timers
     *
     * @param timerToBeRemoved the timer that will be removed from the data structure
     * @param registeredTimers data structure in which all user registered timers are stored
     * @throws IOException
     */
    private void removeTimerFromDataStructure(TimerInKeyedBroadcastProcess<E> timerToBeRemoved, SortedMap<Long,
            List<TimerInKeyedBroadcastProcess<E>>> registeredTimers) throws IOException {

        List<TimerInKeyedBroadcastProcess<E>> queue = registeredTimers.getOrDefault(timerToBeRemoved.getTime(), null);

        if (queue == null)
            return;
        queue.remove(timerToBeRemoved);

        if (queue.size() == 0)
            registeredTimers.remove(timerToBeRemoved.getTime());

        this.registeredTimersState.update(registeredTimers);
    }


    /**
     * deletes everything about the timer specified with timerToDeleteID (It's ID), if it exists.
     *
     * @param timerToDeleteID the ID of the timer that will be deleted.
     * @param readOnlyContext ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @throws IOException
     */
    public void unregisterTimer(int timerToDeleteID, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) throws IOException {

        if (!isTimerRegistered(timerToDeleteID)) {
            return;
        }

        Map<Integer, TimerInKeyedBroadcastProcess<E>> timersUID = this.timersUIDState.value();
        TimerInKeyedBroadcastProcess<E> timerToBeDeleted = timersUID.get(timerToDeleteID);

        untrackTimer(timerToBeDeleted);

        cleanState(timerToBeDeleted);

        keepConsistentState(readOnlyContext, registeredTimersState.value());
    }

    /**
     * removes the timer passed as an argument from the tracked timers(timersUIDState).
     *
     * @param timerToBeDeleted the timer which will be removed from timersUIDState.
     * @throws IOException
     */
    private void untrackTimer(TimerInKeyedBroadcastProcess<E> timerToBeDeleted) throws IOException {
        Map<Integer, TimerInKeyedBroadcastProcess<E>> timersUID = this.timersUIDState.value();

        timersUID.remove(timerToBeDeleted.getUID());

        this.timersUIDState.update(timersUID);
    }

    /**
     * checks if the there is registered timer with the specified ID.
     *
     * @param timerToDeleteID the Id of the timer.
     * @return true if the timer with the specified Id exists otherwise returns false.
     * @throws IOException
     */
    private boolean isTimerRegistered(int timerToDeleteID) throws IOException {
        Map<Integer, TimerInKeyedBroadcastProcess<E>> timersUID = this.timersUIDState.value();
        return timersUID.containsKey(timerToDeleteID);
    }

    /**
     * maps the ID and the corresponding timer in the map addedTimers.
     *
     * @param timerToTrack the timer that will be tracked with it's id.
     * @throws IOException
     */
    private void trackTimer(TimerInKeyedBroadcastProcess<E> timerToTrack) throws IOException {
        Map<Integer, TimerInKeyedBroadcastProcess<E>> addedTimersMap = timersUIDState.value();
        addedTimersMap.put(timerToTrack.getUID(), timerToTrack);
        this.timersUIDState.update(addedTimersMap);
    }

    /**
     * register the timer passed as an argument using Apache Flink's timer service.
     *
     * @param timerToRegisterInFlink the timer that will be officially registered.
     * @param readOnlyContext        ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @throws IOException
     */
    private void registerTimer(TimerInKeyedBroadcastProcess<E> timerToRegisterInFlink, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) throws IOException {
        if (this.eventTime) {
            readOnlyContext.timerService().registerEventTimeTimer(timerToRegisterInFlink.getTime());
        } else {
            readOnlyContext.timerService().registerProcessingTimeTimer(timerToRegisterInFlink.getTime());
        }
    }


    /**
     * checks if any unofficially registered (not registered in Flink's timer service) timer needs to be fired
     * if yes, then call call artificialOnTimer with the timer as argument.
     *
     * @param context OnTimerContext in Flink's KeyedBroadcastProcessFunction.
     * @param out     Collector in Flink's KeyedBroadcastProcessFunction.
     * @throws IOException
     */
    private void fireExpiredTimers(KeyedBroadcastProcessFunction.OnTimerContext context, Collector<E> out) throws IOException {

        long currentTime = this.getCurrentTime(context);
        SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>> timersQueue = this.registeredTimersState.value();

        if (timersQueue.size() == 0)
            return;

        timersQueue.keySet().stream().filter(triggeringTime -> triggeringTime < currentTime).map(timersQueue::get)
                .forEach(timersQueueForFiring -> this.fireAllTimers(timersQueueForFiring, context, out));

        List<Long> expiredTriggeringTimes = timersQueue.keySet().stream().filter(triggerTime -> triggerTime < currentTime)
                .collect(Collectors.toList());

        expiredTriggeringTimes.stream().forEach(tirggeringTime -> timersQueue.remove(tirggeringTime));

        this.registeredTimersState.update(timersQueue);
    }

    /**
     * fires all the timers that are in the passed list timersQueueForFiring
     *
     * @param timersQueueForFiring a list containing timers that will are triggered and will be fired
     * @param ctx                  OnTimerContext in Flink's KeyedBroadcastProcessFunction.
     * @param out                  Collector in Flink's KeyedBroadcastProcessFunction.
     */
    private void fireAllTimers(List<TimerInKeyedBroadcastProcess<E>> timersQueueForFiring,
                               KeyedBroadcastProcessFunction.OnTimerContext ctx,
                               Collector<E> out) {
        timersQueueForFiring.forEach(timerForFiring -> {
            try {
                timerForFiring.fire(ctx, out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * The main logic of the timer management is done here.
     * Checks if there is already registered timer in Flink's timer service at the same timestamp as the new timer triggering timestamp
     * if not then registers the new timer in Flink's timer service and adds it to the list, otherwise just adds it to the list of timers with
     * same triggering timestamp
     *
     * @param readOnlyContext Flink's Keyed Broadcast Process function Context.
     * @throws IOException
     */
    private void addTimer(TimerInKeyedBroadcastProcess<E> newTimer, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) throws IOException {
        //timers that are psudo registered (registered in the timer menager)
        SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>> registeredTimers = this.registeredTimersState.value();

        //if timer with this timestamp has not been registered in Flink timer service so far
        if (!registeredTimers.containsKey(newTimer.getTime())) {
            //timer that is officially registered in Flink's timer service
            this.registerTimer(newTimer, readOnlyContext);
            newTimer.setOfficial(Boolean.TRUE);
        }

        registeredTimers.computeIfPresent(newTimer.getTime(), (timer, queue) -> {
            queue.add(newTimer);
            return queue;
        });
        registeredTimers.computeIfAbsent(newTimer.getTime(), (timer) -> {
            ArrayList<TimerInKeyedBroadcastProcess<E>> l = new ArrayList<>();
            l.add(newTimer);
            return l;
        });

        this.registeredTimersState.update(registeredTimers);

    }

    /**
     * makes sure that for all different triggering times in the map there is officially registered timer
     *
     * @param readOnlyContext  ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @param registeredTimers SortedMap in which all the timers are stored (triggering time-> list of timers that should
     *                         fire at that timestamp), and sorted by their triggering time
     */
    private void keepConsistentState(KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext, SortedMap<Long, List<TimerInKeyedBroadcastProcess<E>>> registeredTimers) {
        registeredTimers.entrySet().stream()
                .filter(entry -> {
                    return entry.getValue().stream().noneMatch(TimerInKeyedBroadcastProcess::isOfficial);
                })
                .forEach(entry -> {
                    try {
                        this.registerTimer(entry.getValue().get(FIRST), readOnlyContext);
                        entry.getValue().get(FIRST).setOfficial(Boolean.TRUE);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    /**
     * returns current watermark if time charachteristics are set to event time
     * otherwise returns current processing time.
     *
     * @param readOnlyContext ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @return
     */
    private long getCurrentTime(KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) {
        if (this.eventTime)
            return readOnlyContext.timerService().currentWatermark();
        return readOnlyContext.timerService().currentProcessingTime();
    }


    /**
     * checks if the triggeringTime is valid
     *
     * @param triggeringTime the triggering time that needs to be checked
     * @param readOnlyContext ReadOnlyContext in Flink's KeyedBroadcastProcessFunction.
     * @return
     */
    private boolean isValidTriggeringTime(long triggeringTime, KeyedBroadcastProcessFunction.ReadOnlyContext readOnlyContext) {
        if (triggeringTime < 0) {
            return Boolean.FALSE;
        }
        if (triggeringTime < this.getCurrentTime(readOnlyContext)) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

}

