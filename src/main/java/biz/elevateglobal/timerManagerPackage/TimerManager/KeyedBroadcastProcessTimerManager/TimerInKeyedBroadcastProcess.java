package biz.elevateglobal.timerManagerPackage.TimerManager.KeyedBroadcastProcessTimerManager;

import biz.elevateglobal.timerManagerPackage.OnTimerCallbacks.IOnTimerKeyedBroadcast;
import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;

/**
 * This class represents a timer which is used in Keyed process function.
 *
 * @param <E> - Type of the output elements.
 */
public class TimerInKeyedBroadcastProcess<E> {

    /**
     * the time at which this timer should be fired.
     */
    private long time;

    /**
     * the callback which defines the behaviour which should be executed when this timer fires.
     */
    private IOnTimerKeyedBroadcast<E> iOnTimer;

    private boolean official;

    /**
     * unique ID of the timer, this ID can be used for deleting the registered timer.
     */
    private int uniqueId;

    public TimerInKeyedBroadcastProcess(long time, IOnTimerKeyedBroadcast<E> iOnTimer) {
        this.time = time;
        this.iOnTimer = iOnTimer;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public IOnTimerKeyedBroadcast<E> getiOnTimer() {
        return iOnTimer;
    }

    public void setiOnTimer(IOnTimerKeyedBroadcast<E> iOnTimer) {
        this.iOnTimer = iOnTimer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimerInKeyedBroadcastProcess<?> timer = (TimerInKeyedBroadcastProcess<?>) o;
        return time == timer.time &&
                uniqueId == timer.uniqueId;
    }

    public boolean isOfficial() {
        return official;
    }

    public void setOfficial(boolean official) {
        this.official = official;
    }

    public int getUID() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * execute the defined callback in this timer.
     *
     * @param context OnTimerContext in Flink's KeyedBroacastProcessFunction.
     * @param out     Collector in Flink's KeyedBroacastProcessFunction
     * @throws IOException
     */
    public void fire(KeyedBroadcastProcessFunction.OnTimerContext context, Collector<E> out) throws IOException {
        this.iOnTimer.onTimerFunction(this.time, context, out);
    }
}
