package biz.elevateglobal.timerManagerPackage.OnTimerCallbacks;

import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;


/**
 * functional interface that is used for defining specific callbacks for timers that will be executed when the
 * corresponding timer will fire.
 *
 * @param <K> Type of the output elements.
 */
public interface IOnTimerKeyedProcessCallback<K> {

    /**
     * defines the onTimer behaviour of a timer.
     *
     * @param timestamp The timestamp of the firing timer.
     * @param context   Context in Flink's KeyedProcessFunction.
     * @param out       Collector in Flink's KeyedProcessFunction.
     * @throws IOException
     */
    void onTimerFunction(long timestamp, KeyedProcessFunction.Context context, Collector<K> out) throws IOException;
}