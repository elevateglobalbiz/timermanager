package biz.elevateglobal.timerManagerPackage.OnTimerCallbacks;

import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;

/**
 * functional interface that is used for defining specific callbacks for timers that will be executed when the
 * corresponding timer fires.
 * @param <K> Type of the output elements.
 */
public interface IOnTimerKeyedBroadcast<K> {
    /**
     * defines the onTimer behaviour of a timer.
     * @param timestamp - The timestamp of the firing timer.
     * @param context - Flink's Keyed Process Function Context.
     * @param out - Flink's Keyed Process Function Collector.
     * @throws IOException
     */
    void onTimerFunction(long timestamp, KeyedBroadcastProcessFunction.ReadOnlyContext context, Collector<K> out) throws IOException;

}
