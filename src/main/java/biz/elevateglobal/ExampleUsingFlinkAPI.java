package biz.elevateglobal;

import biz.elevateglobal.messages.Instance;
import biz.elevateglobal.source.DataSource;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.util.Collector;

import javax.annotation.Nullable;

public class ExampleUsingFlinkAPI {

    final static String EVENT_TIME_PARAMETAR = "event_time";
    final static boolean EVENT_TIME_DEFAULT_VALUE = true;

    public static void main(String[] args) throws Exception {

        ParameterTool parameterTool = ParameterTool.fromArgs(args);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(4);

        boolean eventTime = parameterTool.getBoolean(EVENT_TIME_PARAMETAR, EVENT_TIME_DEFAULT_VALUE);
        if (eventTime) {
            env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        } else {
            env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);
        }

        DataStream<Instance> dataSource = env.addSource(new DataSource())
                .assignTimestampsAndWatermarks(new AssignerWithPunctuatedWatermarks<Instance>() {
                    @Nullable
                    @Override
                    public Watermark checkAndGetNextWatermark(Instance lastElement, long extractedTimestamp) {
                        return new Watermark(extractedTimestamp);
                    }

                    @Override
                    public long extractTimestamp(Instance element, long previousElementTimestamp) {
                        return element.getTimestamp();
                    }
                });

        dataSource
                .keyBy(Instance::getKey)
                .process(new KeyedProcessFunction<String, Instance, Instance>() {

                    @Override
                    public void processElement(Instance value, Context ctx, Collector<Instance> out) throws Exception {

                        System.out.println("Instance with timestamp " + value.getTimestamp() + " with key = " + ctx.getCurrentKey());

                        long timer = 0;
                        long currentWatermark = ctx.timerService().currentWatermark();

                        if (value.getTimestamp() % 2 == 0) {

                            timer = currentWatermark + 3;

                        } else {

                            timer = currentWatermark + 10;

                        }
                        ctx.timerService().registerEventTimeTimer(timer);

                        System.out.println("Current watermark " + currentWatermark);

                        System.out.println("Registered timer on " + timer);

                    }

                    @Override
                    public void onTimer(long timestamp, OnTimerContext ctx, Collector<Instance> out) throws Exception {

                        System.out.println("Timer registered at " + timestamp + " has fired");
                    }
                });

        env.execute("timer-manager");
    }
}
